#include<ros/ros.h>
#include<sensor_msgs/Image.h>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

using namespace cv;
using namespace std;

ros::Publisher new_image_raw;

//YUV�摜Y�`�����l�����Ԃ�
Mat getminImage(Mat &src)
{
        Mat minImage = Mat::zeros(src.rows, src.cols, CV_8UC1);
        cvtColor(src, src, CV_BGR2YUV);
        vector<Mat>planes;
        split(src, planes);
        minImage = planes.at(0);
        //namedWindow("Ychannel");
        //imshow("Ychannel", minImage);
        return minImage;
}

//RGB�`�����l���̍ŏ��l�摜
//Mat getminImage(Mat &src)
//{
//	Mat minImage = Mat::zeros(src.rows, src.cols, CV_8UC1);
//	Vec3b color;
//	for (int m = 0; m<src.rows; m++)
//	{
//		for (int n = 0; n<src.cols; n++)
//		{
//			color = src.at<Vec3b>(m, n);
//			minImage.at<uchar>(m, n) = min(min(color.val[0], color.val[1]), color.val[2]);
//		}
//	}
//	return minImage;
//}

//HSV�摜V�`�����l�����Ԃ�
//Mat getminImage(Mat &src)
//{
//	Mat minImage = Mat::zeros(src.rows, src.cols, CV_8UC1);
//	cvtColor(src, src, CV_BGR2HSV);
//	vector<Mat>planes;
//	split(src, planes);
//	minImage = planes.at(2);
//	imshow("HSV", minImage);
//	return minImage;
//}

//���`���̒��ɂ������߂�
//1.�摜�S�̂̉��f�̕��ϒl
//double avgpixel(Mat src)
//{
//	Scalar avg = mean(src);
//	return avg.val[0];
//}

//���l�����߂�
double avgpixel(Mat &src)
{
        double minVal, maxVal;
        minMaxLoc(src, &minVal, &maxVal);
        double avg = (minVal + maxVal) / 2;
        return avg;
}

//�K���}�␳
void GammaCorrection(Mat& src, Mat& dst, float fGamma)
{
        CV_Assert(src.data);
        // accept only char type matrices
        CV_Assert(src.depth() != sizeof(uchar));
        // build look up table
        unsigned char lut[256];
        for (int i = 0; i < 256; i++)
        {
                lut[i] = saturate_cast<uchar>(pow((float)(i / 255.0), fGamma) * 255.0f);
        }
        dst = src.clone();
        const int channels = dst.channels();
        switch (channels)
        {
        case 1:
        {
                          MatIterator_<uchar> it, end;
                          for (it = dst.begin<uchar>(), end = dst.end<uchar>(); it != end; it++)
                                  //*it = pow((float)(((*it))/255.0), fGamma) * 255.0;
                                  *it = lut[(*it)];
                          break;
        }
        case 2:
        {
                          MatIterator_<Vec3b> it, end;
                          for (it = dst.begin<Vec3b>(), end = dst.end<Vec3b>(); it != end; it++)
                          {
                                  (*it)[0] = lut[((*it)[0])];
                                  (*it)[1] = lut[((*it)[1])];
                                  (*it)[2] = lut[((*it)[2])];
                          }
                          break;
        }
        }
}

//���ߐ}t(x)�����߂�
Mat transmission(Mat src, Mat Mmed, int a)
{
        Mat t = Mat::zeros(src.rows, src.cols, CV_8UC3);
        double m, p, q, k;
        m = avgpixel(src) / 255;//���l�͉摜�S�̖̂��Â������ł���
        p = 1.3;//�����ɂ�����p=1.3���K�؂����qp�͖������̒��x�������ł���
        q = 1 + (m - 0.5);//���qq�͉摜�̌��x�𒲐������Bm>0.5�̏ꍇ�A�Z���Bm<0.5�̏ꍇ�A�����B
        k = min(m*p*q, 0.95);//�[�x���ۑ����邽�߂ɁA�ő��l0.95���ݒ肷��
        t = 255 * (1 - k*Mmed / a);
        GammaCorrection(t, t, 1.3 - m);
        //printf("m=%f\n", m);
        //imshow("���ߐ}", t);
        return t;
}
//Airlight�����߂��AY�`�����l���摜�̒��Ɉ��Ԗ��邢�_���I���ŁA���f�l��A�Ƃ���
int Airlight(Mat& YChannel)
{
        double minDC, maxDC;
        minMaxLoc(YChannel, &minDC, &maxDC);
        cout << "estimated airlight of a =" << maxDC << endl;
        return maxDC;
}
//�摜�𕜌����f���ɑ�������
Mat getDehazed(Mat &src, Mat &t, int a)
{
        double tmin = 0.1;
        double tmax;
        Scalar inttran;
        Vec3b intsrc;
        Mat dehazed = Mat::zeros(src.rows, src.cols, CV_8UC3);
        for (int i = 0; i<src.rows; i++)
        {
                for (int j = 0; j<src.cols; j++)
                {
                        inttran = t.at<uchar>(i, j);
                        intsrc = src.at<Vec3b>(i, j);
                        tmax = (inttran.val[0] / 255) < tmin ? tmin : (inttran.val[0] / 255);
                        for (int k = 0; k<3; k++)
                        {
                                dehazed.at<Vec3b>(i, j)[k] = abs((intsrc.val[k] - a) / tmax + a) > 255 ? 255 : abs((intsrc.val[k] - a) / tmax + a);
                        }
                }
        }
        return dehazed;
}

static void get_image_raw_callback(const sensor_msgs::ImageConstPtr& image_source)
{
    //cv::Mat mat(image_source->height,image_source->width,CV_8UC3,image_source->data.data());
    cv_bridge::CvImagePtr cv_image = cv_bridge::toCvCopy(image_source, "bgr8");
    cv::Mat img = cv_image->image;

    cv::Mat dst, t, minImg, mmed, tmap;
    minImg = getminImage(img);
    medianBlur(minImg, mmed, 5);
    int a = Airlight(minImg);
    t = transmission(img, mmed, a);
    cvtColor(img, img, CV_YUV2BGR);
    //blur(t, t, Size(5,5));
    dst = getDehazed(img, t, a);

    cv_image->image=dst;
    new_image_raw.publish(cv_image->toImageMsg());

    /*sensor_msgs::Image msg;
    msg.header.seq=image_source->header.seq;
    msg.header.stamp=image_source->header.stamp;
    msg.header.frame_id=image_source->header.frame_id;
    msg.height=image_source->height;
    msg.width=image_source->width;
    msg.encoding="bgr8";//image_source->encoding;
    msg.is_bigendian=image_source->is_bigendian;
    msg.step=3*msg.width;
//std::cout<<image_source->encoding<<','<<image_source->width<<std::endl;
    size_t image_size = image_source->step*image_source->height;
    msg.data.resize(image_size);
    memcpy(msg.data.data(), mat_image.ptr(), image_size);
    new_image_raw.publish(msg);*/
}

int main(int argc,char** argv)
{
    ros::init(argc,argv,"image_dehazing");
    ros::NodeHandle n;
    ros::NodeHandle private_nh("~");

    new_image_raw = n.advertise<sensor_msgs::Image>("output_image_raw", 10);
    ros::Subscriber get_image_raw=n.subscribe("input_image_raw",10,get_image_raw_callback);

    while(ros::ok())
    {
            ros::spinOnce();
    }
    return 0;
}

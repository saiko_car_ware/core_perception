#include<ros/ros.h>
#include<sensor_msgs/Image.h>

ros::Publisher new_image_raw;

static void get_image_raw_callback(const sensor_msgs::Image& image_source)
{
    sensor_msgs::Image msg;
    msg.header.seq=image_source.header.seq;
    msg.header.stamp=image_source.header.stamp;
    msg.header.frame_id=image_source.header.frame_id;
    msg.height=image_source.height;
    msg.width=image_source.width;
    msg.encoding=image_source.encoding;
    msg.is_bigendian=image_source.is_bigendian;
    msg.step=image_source.step;

    size_t image_size = image_source.step*image_source.height;
    msg.data.resize(image_size);
    memcpy(msg.data.data(), image_source.data.data(), image_size);
    new_image_raw.publish(msg);
}

int main(int argc,char** argv)
{
    ros::init(argc,argv,"image_dehazing");
    ros::NodeHandle n;
    ros::NodeHandle private_nh("~");

    new_image_raw = n.advertise<sensor_msgs::Image>("output_image_raw", 10);
    ros::Subscriber get_image_raw=n.subscribe("input_image_raw",10,get_image_raw_callback);
    while(ros::ok())
    {
            ros::spinOnce();
    }
    return 0;
}

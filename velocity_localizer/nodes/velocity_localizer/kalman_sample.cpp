#include <ros/ros.h>
#include <std_msgs/Int32.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <sensor_msgs/Imu.h>
#include <autoware_can_msgs/MicroBusCan502.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

/*class KalmanFilterResult
{
private:
    double filtered_status_;
    double correct_prediction_error_disp_;
public:
    KalmanFilterResult(double filtered_status, double correct_prediction_error_disp)
        : filtered_status_(filtered_status)
        , correct_prediction_error_disp_(correct_prediction_error_disp)
    {
    }
    double getFilteredStatus() {return filtered_status_;}
    double getCorrectPredictionErrorDisp() {return correct_prediction_error_disp_;}
};*/

class KalmanFilter
{
private:
    double status_;           //状態
    double prediction_error_disp_; //状態の予測誤差の分散

public:
    KalmanFilter(double first_status, double prediction_error_disp)
        : status_(first_status)
        , prediction_error_disp_(prediction_error_disp)
    {
    }

    //observation　　　        :当期の観測値
    //equation_status_sigma    :状態方程式のノイズの分散
    //prediction_status_sigma　:観測方程式のノイズの分散
    void local_level_model(double observation, double equation_status_sigma, double prediction_status_sigma)
    {
        //状態の予測(ローカルレベルモデルなので、予測値は、前期の値と同じ)
        double status_now = status_;
        //状態の予測誤差の分散
        double prediction_error_disp_now = prediction_error_disp_ + equation_status_sigma;
        //カルマンゲイン
        double kGain = prediction_error_disp_now / (prediction_error_disp_now + prediction_status_sigma);
        //カルマンゲインを使って補正された状態
        double filtered_status = status_now + kGain * (observation - status_now);
        //正された状態の予測誤差の分散
        double correct_prediction_error_disp = (1 - kGain) * prediction_error_disp_now;

        status_ = filtered_status;
        prediction_error_disp_ = correct_prediction_error_disp;
        //return KalmanFilterResult(filtered_status, correct_prediction_error_disp);
    }
    /*KalmanFilterResult local_level_model(double observation, double status_prev, double prediction_error_disp_prev,
                           double equation_status_sigma, double prediction_status_sigma)
    {
        //状態の予測(ローカルレベルモデルなので、予測値は、前期の値と同じ)
        double status_now = status_prev;
        //状態の予測誤差の分散
        double prediction_error_disp = prediction_error_disp_prev + equation_status_sigma;
        //カルマンゲイン
        double kGain = prediction_error_disp / (prediction_error_disp + prediction_status_sigma);
        //カルマンゲインを使って補正された状態
        double filtered_status = status_now + kGain * (observation - status_now);
        //正された状態の予測誤差の分散
        double correct_prediction_error_disp = (1 - kGain) * prediction_error_disp;

        return KalmanFilterResult(filtered_status, correct_prediction_error_disp);
    }*/

    double getStatus() {return status_;}
    double getPredictionErrorDisp() {return prediction_error_disp_;}
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "velocity_localizer");
	ros::NodeHandle nh;
	ros::NodeHandle private_nh("~");

    std::vector<double> nairu = {1120,1160,963,1210,1160,1160,813,1230,1370,1140,995,935,1110,994,1020,960,1180,799,958,1140,1100,
                      1210,1150,1250,1260,1220,1030,1100,774,840,874,694,940,833,701,916,692,1020,1050,969,831,726,
                      456,824,702,1120,1100,832,764,821,768,845,864,862,698,845,744,796,1040,759,781,865,845,
                      944,984,897,822,1010,771,676,649,846,812,742,801,1040,860,874,848,890,744,749,838,1050,
                      918,986,797,923,975,815,1020,906,901,1170,912,746,919,718,714,740};
    KalmanFilter kalman(0.0, 1000.0);
    for(int cou=0; cou<nairu.size(); cou++)
    {
        kalman.local_level_model(nairu[cou], 10000.0, 1000.0);
        std::cout << nairu[cou] << "," << kalman.getStatus() << "," << kalman.getPredictionErrorDisp() << std::endl;
    }
    ros::spin();
    return 0;
}
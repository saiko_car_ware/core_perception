#include <ros/ros.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float64.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <can_msgs/Frame.h>
#include <sensor_msgs/Imu.h>
#include <autoware_config_msgs/ConfigVelocityLocalizer.h>
#include <nav_msgs/Odometry.h>
#include <autoware_msgs/Lane.h>
#include <autoware_can_msgs/MicroBusCan502.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <mobileye_560_660_msgs/AftermarketLane.h>
#include <mobileye_560_660_msgs/ObstacleData.h>

static const int SYNC_FRAMES = 50;
typedef message_filters::sync_policies::ApproximateTime<geometry_msgs::PoseStamped, geometry_msgs::TwistStamped>
    PoseTwistSync;

class VelocityLocalizer
{
private:
    const std::string base_link_name = "velocity_base_link";

    ros::NodeHandle nh_, pnh_;

    ros::Publisher pub_velocity_pose_;
    ros::Subscriber sub_base_waypoints_, sub_localizer_select_num_, sub_odom_;

    message_filters::Subscriber<geometry_msgs::PoseStamped> *sub_current_pose_;
    message_filters::Subscriber<geometry_msgs::TwistStamped> *sub_current_velocity_;
    message_filters::Synchronizer<PoseTwistSync> *sync_tp_;

    tf::TransformBroadcaster tf_broadcaster_;
    tf::TransformListener tf_listener_;

    autoware_config_msgs::ConfigVelocityLocalizer config_;
    double tf_vx_, tf_vy_, tf_vz_, tf_vroll_, tf_vpitch_, tf_vyaw_;//runtime manager読み込み時のsetupタブtfの数値
    autoware_msgs::Lane base_waypoints_;//lane_selectノードがpublishしている単一レーン
    int localizer_select_num_;//localizer_switchの現在のlocalizerの種類
    geometry_msgs::PoseStamped base_pose_;//このノードのlocalizerが機能しているときのベースpose
    geometry_msgs::TwistStamped base_twist_;//このノードのlocalizerが機能しているときのベースtwist
    double distance_to_waypoints_;//base_poseと経路との距離

    double euclidean_distance(geometry_msgs::Pose pose, geometry_msgs::Pose cur)
    {
        double x = pose.position.x - cur.position.x;
        double y = pose.position.y - cur.position.y;
        double z = pose.position.z - cur.position.z;
        return sqrt(x*x + y*y + z*z);
    }

    geometry_msgs::Pose crossing_point(geometry_msgs::Pose pose1, geometry_msgs::Pose pose2, geometry_msgs::Pose cur)
    {
        double x0 = cur.position.x, x1 = pose1.position.x, x2 = pose2.position.x;
        double y0 = cur.position.y, y1 = pose1.position.y, y2 = pose2.position.y;
        double a = y2 - y1;
        double b = x1 - x2;
        double c = - x1 * y2 + y1 * x2;

        double y = -(a*b*x0 - a*a*y0 + b*c) / (a*a + b*b);
        double x = -y*b/a - c/a;

        geometry_msgs::Pose ret;
        ret.position.x = x;
        ret.position.y = y;
        ret.position.z = pose1.position.z;
        ret.orientation = pose1.orientation;
        return ret;
    }

    void publish_base_tf()
    {
        if(base_waypoints_.waypoints.size() != 0 && base_pose_.header.frame_id != "" && base_twist_.header.frame_id != "")
        {
            tf::Vector3 origin(base_pose_.pose.position.x, base_pose_.pose.position.y, base_pose_.pose.position.z);
            tf::Quaternion qua(base_pose_.pose.orientation.x, base_pose_.pose.orientation.y, base_pose_.pose.orientation.z, base_pose_.pose.orientation.w);
            tf::Transform transform;
            transform.setOrigin(origin);
            transform.setRotation(qua);
            tf_broadcaster_.sendTransform(tf::StampedTransform(transform, base_pose_.header.stamp, "/map", "/waypoints_base_link1"));
        }
    }

    void callbackBaseWaypoints(const autoware_msgs::Lane &msg)
    {
        if(localizer_select_num_ != 2)
        {
            publish_base_tf();
        }
        base_waypoints_ = msg;
    }

    void callbackPoseTwist(const geometry_msgs::PoseStampedConstPtr &pose, const geometry_msgs::TwistStampedConstPtr &twist)
    {
        if(localizer_select_num_ != 2)
        {
            if(base_waypoints_.waypoints.size() > 1)
            {
                int ind_min = 1;
                double dist_min = DBL_MAX;
                for(int i=0; i<base_waypoints_.waypoints.size()-1; i++)
                {
                    double dist = euclidean_distance(base_waypoints_.waypoints[i].pose.pose, base_pose_.pose);
                    if(dist < dist_min)
                    {
                        ind_min = i;
                        dist_min = dist;
                    }
                }

                geometry_msgs::Pose pose1 = base_waypoints_.waypoints[ind_min].pose.pose;
                geometry_msgs::Pose pose2 = base_waypoints_.waypoints[ind_min+1].pose.pose;
                geometry_msgs::Pose pose_cross = crossing_point(pose1, pose2, base_pose_.pose);
                geometry_msgs::PoseStamped velocity_pose;
                velocity_pose.header.frame_id = "/map";
                velocity_pose.header.stamp = pose->header.stamp;
                velocity_pose.pose = pose_cross;
                pub_velocity_pose_.publish(velocity_pose);

                double tf_x = pose_cross.position.x;
                double tf_y = pose_cross.position.y;
                double tf_z = pose_cross.position.z;
                tf::Vector3 tf_origin(pose_cross.position.x, pose_cross.position.y, pose_cross.position.z);
                tf::Quaternion tf_qua(pose_cross.orientation.x, pose_cross.orientation.y, pose_cross.orientation.z, pose_cross.orientation.w);
                tf::Transform transform;
                transform.setOrigin(tf_origin);
                transform.setRotation(tf_qua);
                tf_broadcaster_.sendTransform(tf::StampedTransform(transform, base_pose_.header.stamp, "/map", "/waypoints_base_link2"));

                distance_to_waypoints_ = dist_min;
            }
            base_pose_ = *pose;  base_twist_ = *twist;
            base_twist_.header.frame_id = base_link_name;
            publish_base_tf();
        }
    }

    void callbackLocalizerSelectNum(const std_msgs::Int32 &msg)
    {
        localizer_select_num_ = msg.data;
        std::cout << localizer_select_num_ << std::endl;
    }

    void callbackOdom(const nav_msgs::Odometry &msg)
    {
        if(localizer_select_num_ == 2)
        {

        }
    }

    void callbackConfig(const autoware_config_msgs::ConfigVelocityLocalizer &msg)
    {
        config_ = msg;
    }
public:
    VelocityLocalizer(ros::NodeHandle nh, ros::NodeHandle pnh)
        : nh_(nh)
        , pnh_(pnh)
    {
        
        if (nh.getParam("tf_x", tf_vx_) == false)
        {
          std::cout << "tf_x is not set." << std::endl;
          return;
        }
        if (nh.getParam("tf_y", tf_vy_) == false)
        {
          std::cout << "tf_y is not set." << std::endl;
          return;
        }
        if (nh.getParam("tf_z", tf_vz_) == false)
        {
          std::cout << "tf_z is not set." << std::endl;
          return;
        }
        if (nh.getParam("tf_roll", tf_vroll_) == false)
        {
          std::cout << "tf_roll is not set." << std::endl;
          return;
        }
        if (nh.getParam("tf_pitch", tf_vpitch_) == false)
        {
          std::cout << "tf_pitch is not set." << std::endl;
          return;
        }
        if (nh.getParam("tf_yaw", tf_vyaw_) == false)
        {
          std::cout << "tf_yaw is not set." << std::endl;
          return;
        }

        pub_velocity_pose_ = nh_.advertise<geometry_msgs::PoseStamped>("/velocity_pose", 1);

        sub_base_waypoints_ = nh.subscribe("/base_waypoints", 1, &VelocityLocalizer::callbackBaseWaypoints, this);
        sub_localizer_select_num_ = nh.subscribe("/localizer_select_num", 1, &VelocityLocalizer::callbackLocalizerSelectNum, this);
        sub_odom_ = nh.subscribe("/vehicle/odom", 1, &VelocityLocalizer::callbackOdom, this);

        sub_current_pose_ = new message_filters::Subscriber<geometry_msgs::PoseStamped>(nh_, "/current_pose", 1);
        sub_current_velocity_ = new message_filters::Subscriber<geometry_msgs::TwistStamped>(nh_, "/current_velocity", 1);
        sync_tp_ = new message_filters::Synchronizer<PoseTwistSync>(PoseTwistSync(SYNC_FRAMES), *sub_current_pose_, *sub_current_velocity_);
        sync_tp_->registerCallback(boost::bind(&VelocityLocalizer::callbackPoseTwist, this, _1, _2));

        base_pose_.header.frame_id = "";//base_pose_を未入力扱い
        base_twist_.header.frame_id = "";//base_twist_を未入力扱い
        config_.distance_to_waypoints = 0.3;
    }

    ~VelocityLocalizer()
    {
    }
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "velocity_localizer");
	ros::NodeHandle nh;
	ros::NodeHandle private_nh("~");

    VelocityLocalizer velocity_localizer(nh, private_nh);
    ros::spin();
    return 0;
}
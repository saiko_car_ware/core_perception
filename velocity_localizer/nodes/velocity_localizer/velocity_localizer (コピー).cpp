#include <ros/ros.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float64.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <can_msgs/Frame.h>
#include <sensor_msgs/Imu.h>
#include <nav_msgs/Odometry.h>
#include <autoware_msgs/Lane.h>
#include <autoware_can_msgs/MicroBusCan502.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <mobileye_560_660_msgs/AftermarketLane.h>
#include <mobileye_560_660_msgs/ObstacleData.h>

static const int SYNC_FRAMES = 50;
typedef message_filters::sync_policies::ApproximateTime<geometry_msgs::PoseStamped, geometry_msgs::TwistStamped>
    PoseTwistSync;

class VelocityLocalizer
{
private:
    ros::NodeHandle nh_, pnh_;

    message_filters::Subscriber<geometry_msgs::PoseStamped> *sub_current_pose_;
    message_filters::Subscriber<geometry_msgs::TwistStamped> *sub_current_velocity_;
    message_filters::Synchronizer<PoseTwistSync> *sync_tp_;

    ros::Subscriber sub_can502_, sub_localizer_select_num_, sub_imu_, sub_base_waypoints_, sub_odom_;
    ros::Subscriber sub_mobileye_frame_;
    ros::Publisher pub_velocity_base_link_, pub_velocity_twist_, pub_velocity_localizer_, pub_mileage_;

    tf::TransformBroadcaster tf_broadcaster_;
    tf::TransformListener tf_listener_;

    double tf_vx_, tf_vy_, tf_vz_, tf_vroll_, tf_vpitch_, tf_vyaw_;
    geometry_msgs::PoseStamped current_pose_;
    geometry_msgs::TwistStamped current_velocity_;
    sensor_msgs::Imu imu_;
    autoware_can_msgs::MicroBusCan502 can502_;
    nav_msgs::Odometry odometry_;
    int localizer_select_num_;
    ros::Time tf_publish_time_;
    autoware_msgs::Lane base_waypoints_;
    //tf::Transform tf_waypoints_base_, tf_waypoints_second_;
    int waypoints_base_index_;
    bool check_initTF_;
    mobileye_560_660_msgs::AftermarketLane mobileye_lane_;

    //tf::Transform transform_;

    /*void publishWaypointBase(ros::Time time)
    {
        tf_broadcaster_.sendTransform(tf::StampedTransform(tf_waypoints_base_, time, "/map", "/waypoints_base_point"));
        tf_broadcaster_.sendTransform(tf::StampedTransform(tf_waypoints_second_, time, "/waypoints_base_point", "/waypoints_second_point"));
    }*/

    const bool getMessage_bool(const unsigned char *buf, unsigned int bit)
    {
        unsigned long long mask=1;
        mask<<=bit;
        unsigned long long *msgL=(unsigned long long)buf;
        if((*msgL & mask)) return true;
        else return false;
    }

    template<typename T>
    const T getMessage_bit(const unsigned char *buf, const unsigned int lowBit, const unsigned int highBit)
    {
        const unsigned int maxBitSize=sizeof(unsigned long long)*8;
        unsigned long long *msgL=(unsigned long long)buf;
        unsigned long long val=(*msgL)<<maxBitSize-highBit-1;
        unsigned int lowPos=lowBit+(maxBitSize-highBit-1);
        val>>=lowPos;
        return (T)val;
    }

	void callbackMobileyeCan(const can_msgs::Frame &frame)
	{
		switch(frame.id)
		{
		case 0x669:
			{
				if(frame.is_error == false && frame.dlc == 8)
				{
					const unsigned char *buf = (unsigned char*)frame.data.data();
					//Lane type
					mobileye_lane_.lane_type_left = getMessage_bit<unsigned char>(&buf[0], 4, 7);
					mobileye_lane_.lane_type_right = getMessage_bit<unsigned char>(&buf[5], 4, 7);
					//ldw_available
					mobileye_lane_.ldw_available_left = getMessage_bool(&buf[0], 2);
					mobileye_lane_.ldw_available_right = getMessage_bool(&buf[5], 2);
					//lane_confidence
					mobileye_lane_.lane_confidence_left = getMessage_bit<unsigned char>(&buf[0], 0, 1);
					mobileye_lane_.lane_confidence_right = getMessage_bit<unsigned char>(&buf[5], 0, 1);
					//distance_to lane
					int16_t distL, distR;
					unsigned char* distL_p = (unsigned char*)&distL;
					distL_p[1] = getMessage_bit<unsigned char>(&buf[2], 4, 7);
					distL_p[0] = getMessage_bit<unsigned char>(&buf[2], 0, 3) << 4;
					distL_p[0] |= getMessage_bit<unsigned char>(&buf[1], 4, 7);
					if(distL_p[1] & 0x8)//12bitのマイナスか
					{
						distL--;
						distL = ~distL;
						distL_p[1] &= 0x0F;
						distL = -distL;
					}
					mobileye_lane_.distance_to_left_lane = distL * 0.02;
					//std::cout << "distL : " << (int)distL << std::endl;
					unsigned char* distR_p = (unsigned char*)&distR;
					distR_p[1] = getMessage_bit<unsigned char>(&buf[7], 4, 7);
					distR_p[0] = getMessage_bit<unsigned char>(&buf[7], 0, 3) << 4;
					distR_p[0] |= getMessage_bit<unsigned char>(&buf[6], 4, 7);
					if(distR_p[1] & 0x8)//12bitのマイナスか
					{
						distR--;
						distR = ~distR;
						distR_p[1] &= 0x0F;
						distR = -distR;
					}
					mobileye_lane_.distance_to_right_lane = distR * 0.02;
					//std::cout << "distR : " << (int)distR << std::endl;
				}
				break;
			}
		}
	}

    void publishBaseLinkTF(int base_index, ros::Time time)
    {
        geometry_msgs::Point waypoints_base_pose = base_waypoints_.waypoints[base_index].pose.pose.position;
        geometry_msgs::Quaternion waypoints_base_qua = base_waypoints_.waypoints[base_index].pose.pose.orientation;
        tf::Quaternion waypoints_base_qua_tf = tf::Quaternion(waypoints_base_qua.x, waypoints_base_qua.y, waypoints_base_qua.z, waypoints_base_qua.w);
        
        tf::Transform tf_waypoints_base, tf_waypoints_second;
        tf_waypoints_base.setOrigin(tf::Vector3(waypoints_base_pose.x, waypoints_base_pose.y, waypoints_base_pose.z));
        tf_waypoints_base.setRotation(waypoints_base_qua_tf);
        tf_broadcaster_.sendTransform(tf::StampedTransform(tf_waypoints_base, time, "/map", "/waypoints_base_point"));

        tf::Transform tf_current_pose;
        tf_current_pose.setOrigin(tf::Vector3(current_pose_.pose.position.x, current_pose_.pose.position.y, current_pose_.pose.position.z));
        tf_current_pose.setRotation(tf::Quaternion(current_pose_.pose.orientation.x, current_pose_.pose.orientation.y,
                                                   current_pose_.pose.orientation.z, current_pose_.pose.orientation.w));
        tf_broadcaster_.sendTransform(tf::StampedTransform(tf_current_pose, time, "/map", "/waypoints_tmp_point"));

        tf::StampedTransform tf_waypoints_second_stamp;
        tf_listener_.lookupTransform("/waypoints_tmp_point", "/waypoints_base_point", time, tf_waypoints_second_stamp);
        tf_waypoints_second = (tf::Transform)tf_waypoints_second_stamp;
        tf_broadcaster_.sendTransform(tf::StampedTransform(tf_waypoints_second, time, "/waypoints_base_point", "/velocity_base_link"));

        waypoints_base_index_ = base_index;
        tf_publish_time_ = time;
    }

    bool calWaypintsFirstPosition(ros::Time time)
    {
        double min_dt = DBL_MAX;
        int min_ind;

        tf::Vector3 current_vector(current_pose_.pose.position.x, current_pose_.pose.position.y, current_pose_.pose.position.z);
        int base_cou;
        for(base_cou=0; base_cou<base_waypoints_.waypoints.size(); base_cou++)
        {
            tf::Vector3 vector(base_waypoints_.waypoints[base_cou].pose.pose.position.x, base_waypoints_.waypoints[base_cou].pose.pose.position.y,
                                    base_waypoints_.waypoints[base_cou].pose.pose.position.z);
            double dt = tf::tfDistance(current_vector, vector);
            if(min_dt > dt) 
            {
                min_dt = dt;
                min_ind = base_waypoints_.waypoints[base_cou].waypoint_param.id;
            }
        }

        if(base_cou+1 >= base_waypoints_.waypoints.size())
        {
            //error
            return false;
        }
        tf::Vector3 base_vector(base_waypoints_.waypoints[min_ind].pose.pose.position.x, base_waypoints_.waypoints[min_ind].pose.pose.position.y,
                                    base_waypoints_.waypoints[min_ind].pose.pose.position.z);
        tf::Vector3 next_vector(base_waypoints_.waypoints[min_ind+1].pose.pose.position.x, base_waypoints_.waypoints[min_ind+1].pose.pose.position.y,
                                    base_waypoints_.waypoints[min_ind+1].pose.pose.position.z);
        double dt_next = tf::tfDistance(current_vector, next_vector);
        double dt_way = tf::tfDistance(base_vector, next_vector);
        int base_ind = (dt_way > dt_next) ? min_ind-1 : min_ind;
        if(base_ind < 0)
        {
            //error
            return false;
        }

        publishBaseLinkTF(base_ind, time);
        return true;
    }

    void calWaypintsSecondPosition()
    {

    }

    void callbackBaseWaypoints(const autoware_msgs::Lane &msg)
    {
        if(localizer_select_num_ != 2)
        {
            base_waypoints_ = msg;
        }
    }

    const double simpsons_rule(double a, double fa, double b, double fb, double fm)
    {
        double front = (b-a) / 6.0;
        double back = fa + 4.0*fm + fb;
        return front * back;
    }

    double calcDiffForRadian(const double lhs_rad, const double rhs_rad)
    {
      double diff_rad = lhs_rad - rhs_rad;
      if(diff_rad >= M_PI)
         diff_rad = diff_rad - 2*M_PI;
      else if(diff_rad < -M_PI)
         diff_rad = diff_rad + 2*M_PI;
      return diff_rad;
    }

    geometry_msgs::TwistStamped cal_estimate_twist(geometry_msgs::Pose current, geometry_msgs::Pose prev, double speed, ros::Time current_time, ros::Time prev_time)
    {
        double diff_time = (current_time - prev_time).toSec();
        tf::Quaternion qua_cur(current.orientation.x, current.orientation.y, current.orientation.z, current.orientation.w);
        tf::Quaternion qua_pre(prev.orientation.x,    prev.orientation.y,    prev.orientation.z,    prev.orientation.w);
        double roll_cur, roll_prev, pitch_cur, pitch_prev, yaw_cur, yaw_prev;
        tf::Matrix3x3 mat_cur(qua_cur), mat_pre(qua_pre);
        mat_cur.getRPY(roll_cur, roll_cur, yaw_cur);
        mat_pre.getRPY(roll_prev, pitch_prev, yaw_prev);

        double diff_x = current.position.x - prev.position.x;
        double diff_y = current.position.y - prev.position.y;
        double diff_z = current.position.z - prev.position.z;
        double diff_yaw = calcDiffForRadian(yaw_cur, yaw_prev);
        double diff = sqrt(diff_x * diff_x + diff_y * diff_y + diff_z * diff_z);

        double velocity = (diff_time > 0) ? (diff / diff_time) : 0;
        double angular_velocity   = (diff_time > 0) ? (diff_yaw / diff_time) : 0;
        //std::cout<<std::setprecision(20)<<"yaw : "<<diff_yaw<<std::endl;

        geometry_msgs::TwistStamped new_twist;
        new_twist.header.frame_id = "/velocity_base_link";
        new_twist.header.stamp = current_time;
        new_twist.twist.linear.x = speed;
        new_twist.twist.linear.y = 0.0;
        new_twist.twist.linear.z = 0.0;
        new_twist.twist.angular.x = 0.0;
        new_twist.twist.angular.y = 0.0;
        new_twist.twist.angular.z = angular_velocity;
        return new_twist;
    }

    const void publish_can_bask_link(geometry_msgs::PoseStamped new_pose, geometry_msgs::TwistStamped new_twist)
    {
        current_pose_ = new_pose;
        pub_velocity_base_link_.publish(current_pose_);

        current_velocity_ = new_twist;
        pub_velocity_twist_.publish(current_velocity_);
    }

    void publishLocalizer(ros::Time time)
    {
        tf::StampedTransform set_tf_localizer;
        set_tf_localizer.setOrigin(tf::Vector3(tf_vx_, tf_vy_, tf_vz_));
        set_tf_localizer.setRotation(tf::createQuaternionFromRPY(tf_vroll_, tf_vpitch_, tf_vyaw_));
        tf_broadcaster_.sendTransform(tf::StampedTransform(set_tf_localizer, time, "/velocity_base_link", "/velocity_localizer"));

        tf::StampedTransform get_tf_localizer;
        tf_listener_.waitForTransform("/velocity_localizer", "/map",
                              time, ros::Duration(1.0));
        tf_listener_.lookupTransform("/velocity_localizer", "/map", 
                            time, get_tf_localizer);
        geometry_msgs::PoseStamped new_localizer;
        new_localizer.header.frame_id = "map";
        new_localizer.header.stamp = time;
        new_localizer.pose.position.x = get_tf_localizer.getOrigin().getX();
        new_localizer.pose.position.y = get_tf_localizer.getOrigin().getY();
        new_localizer.pose.position.z = get_tf_localizer.getOrigin().getZ();
        new_localizer.pose.orientation.x = get_tf_localizer.getRotation().getX();
        new_localizer.pose.orientation.y = get_tf_localizer.getRotation().getY();
        new_localizer.pose.orientation.z = get_tf_localizer.getRotation().getZ();
        new_localizer.pose.orientation.w = get_tf_localizer.getRotation().getW();
        pub_velocity_localizer_.publish(new_localizer);
    }

    void PoseTwistCallback(const geometry_msgs::PoseStampedConstPtr &pose, const geometry_msgs::TwistStampedConstPtr &twist)
    {
        if(localizer_select_num_ != 2)
        {
            publish_can_bask_link(*pose, *twist);
            publishLocalizer(pose->header.stamp);
        }
    }

    void callbackImu(const sensor_msgs::Imu &msg)
    {
        if(localizer_select_num_ == 2)
        {
            //current_pose
            /*ros::Duration ros_time_diff = msg.header.stamp - imu_.header.stamp;
            double time_diff = ros_time_diff.sec + ros_time_diff.nsec * 1E-9;

            double x2 = msg.linear_acceleration.x * msg.linear_acceleration.x;
            double y2 = msg.linear_acceleration.y * msg.linear_acceleration.y;
            double z2 = msg.linear_acceleration.z * msg.linear_acceleration.z;
            double acc = sqrt(x2*x2 + y2*y2 * z2*z2);

            double va = 0,  vb = time_diff;
            double vfa = imu_.linear_acceleration.x,  vfb = msg.linear_acceleration.x;
            double vpm = (vfa + vfb) / 2.0;
            double distance_v = simpsons_rule(va, vfa, vb, vfb, vpm);

            try
            {
                //tf::Pose vtf;
                //vtf.setOrigin(distance_v, 0, 0);
                tf::Transform velocity_base_link2;
                velocity_base_link2.setOrigin(tf::Vector3(distance_v, 0, 0));
                velocity_base_link2.setRotation(tf::createQuaternionFromRPY(0, 0, 0));
                tf_broadcaster_.sendTransform(tf::StampedTransform(velocity_base_link2, msg.header.stamp, "/velocity_base_link", "/velocity_base_link2"));

                tf::StampedTransform trans;
                tf_listener_.lookupTransform("/velocity_base_link2", "/map", 
				                  msg.header.stamp, trans);
                geometry_msgs::PoseStamped new_current_pose;
                new_current_pose.header.frame_id = "map";
                new_current_pose.header.stamp = msg.header.stamp;
                new_current_pose.pose.position.x = trans.getOrigin().getX();
                new_current_pose.pose.position.y = trans.getOrigin().getY();
                new_current_pose.pose.position.z = trans.getOrigin().getZ();
                new_current_pose.pose.orientation.x = trans.getRotation().getX();
                new_current_pose.pose.orientation.y = trans.getRotation().getY();
                new_current_pose.pose.orientation.z = trans.getRotation().getZ();
                new_current_pose.pose.orientation.w = trans.getRotation().getW();

                //cal_estimate_twist(new_current_pose, current_pose_, );

                //publish_can_bask_link(msg.header.stamp);
            }
            catch(tf::TransformException ex)
            {

            }*/

            //current_pose
            /*ros::Duration ros_time_diff = msg.header.stamp - imu_.header.stamp;
            double time_diff = ros_time_diff.sec + ros_time_diff.nsec * 1E-9;

            double vax = 0,  vbx = time_diff;
            double vfax = imu_.linear_acceleration.x,  vfbx = msg.linear_acceleration.x;
            double vpmx = (vfax + vfbx) / 2.0;
            double distance_vx = simpsons_rule(vax, vfax, vbx, vfbx, vpmx);
            
            double vay = 0,  vby = time_diff;
            double vfay = imu_.linear_acceleration.y,  vfby = msg.linear_acceleration.y;
            double vpmy = (vfay + vfby) / 2.0;
            double distance_vy = simpsons_rule(vay, vfay, vby, vfby, vpmy);

            double vaz = 0,  vbz = time_diff;
            double vfaz = imu_.linear_acceleration.z,  vfbz = msg.linear_acceleration.z;
            double vpmz = (vfaz + vfbz) / 2.0;
            double distance_vz = simpsons_rule(vaz, vfaz, vbz, vfbz, vpmz);

            current_pose_.pose.position.x += distance_vx;
            current_pose_.pose.position.y += distance_vy;
            current_pose_.pose.position.z += distance_vz;

            double aax = 0,  abx = time_diff;
            double afax = imu_.angular_velocity.x,  afbx = msg.angular_velocity.x;
            double apmx = (afax + afbx) / 2.0;
            double distance_ax = simpsons_rule(aax, afax, abx, afbx, apmx);

            double aay = 0,  aby = time_diff;
            double afay = imu_.angular_velocity.y,  afby = msg.angular_velocity.y;
            double apmy = (afay + afby) / 2.0;
            double distance_ay = simpsons_rule(aay, afay, aby, afby, apmy);

            double aaz = 0,  abz = time_diff;
            double afaz = imu_.angular_velocity.z,  afbz = msg.angular_velocity.z;
            double apmz = (afaz + afbz) / 2.0;
            double distance_az = simpsons_rule(aaz, afaz, abz, afbz, apmz);

            double cur_roll, cur_pitch, cur_yaw;
            tf::Quaternion qua(current_pose_.pose.orientation.x, current_pose_.pose.orientation.y,
                               current_pose_.pose.orientation.z, current_pose_.pose.orientation.w);
            tf::Matrix3x3(qua).getRPY(cur_roll, cur_pitch, cur_yaw);
            cur_roll += distance_ax;  cur_pitch += distance_ay;  cur_yaw += distance_az;
            while(cur_roll >= 2*M_PI) cur_roll -=2*M_PI;
            while(cur_roll < 0) cur_roll += 2*M_PI;
            while(cur_pitch >= 2*M_PI) cur_pitch -=2*M_PI;
            while(cur_pitch < 0) cur_pitch += 2*M_PI;
            while(cur_yaw >= 2*M_PI) cur_yaw -=2*M_PI;
            while(cur_yaw < 0) cur_yaw += 2*M_PI;
            tf::Quaternion qua_ret = tf::createQuaternionFromRPY(cur_roll, cur_pitch, cur_yaw);
            current_pose_.pose.orientation.x = qua_ret.getX();
            current_pose_.pose.orientation.y = qua_ret.getY();
            current_pose_.pose.orientation.z = qua_ret.getZ();
            current_pose_.pose.orientation.w = qua_ret.getW();
            //tf::Quaternion qua_plus;
            //qua_plus.setRPY(distance_ax, distance_ay, distance_az);
            //tf::Quaternion qua_cur(current_pose_.pose.orientation.x, current_pose_.pose.orientation.y,
            //                       current_pose_.pose.orientation.z, current_pose_.pose.orientation.w);


            //current_velocity
            current_velocity_.twist.linear.x = msg.linear_acceleration.x;
            current_velocity_.twist.linear.y = msg.linear_acceleration.y;
            current_velocity_.twist.linear.z = msg.linear_acceleration.z;
            current_velocity_.twist.angular.x = msg.angular_velocity.x;
            current_velocity_.twist.angular.y = msg.angular_velocity.y;
            current_velocity_.twist.angular.z = msg.angular_velocity.z;

            publish_can_bask_link(msg.header.stamp);*/
        }

        imu_ = msg;
    }

    double wayBetweenDistance(int index)
    {
        geometry_msgs::Point p1 = base_waypoints_.waypoints[index].pose.pose.position;
        geometry_msgs::Point p2 = base_waypoints_.waypoints[index+1].pose.pose.position;
        tf::Point tp1(p1.x, p1.y, p1.z);
        tf::Point tp2(p2.x, p2.y, p2.z);
        return tf::tfDistance(tp1, tp2);
    }

    void callbackCan502(const autoware_can_msgs::MicroBusCan502 &msg)
    {
        /*ros::Duration ros_time_diff = msg.header.stamp - imu_.header.stamp;
        double time_diff = ros_time_diff.sec + ros_time_diff.nsec * 1E-9;

        double va = 0,  vb = time_diff;
        double vfa = can502_.velocity_mps,  vfb = msg.velocity_mps;
        double vpm = (vfa + vfb) / 2.0;
        double distance_v = simpsons_rule(va, vfa, vb, vfb, vpm);

        std_msgs::Float64 msg_mileage;
        msg_mileage.data = distance_v;
        pub_mileage_.publish(msg_mileage);

        if(localizer_select_num_ == 2 && waypoints_base_index_ >= 0 && waypoints_base_index_ < base_waypoints_.waypoints.size()-1)
        {
            try
            {
                for(int ind=waypoints_base_index_; ind<base_waypoints_.waypoints.size()-1; ind++)
                {
                    double baseX = base_waypoints_.waypoints[ind].pose.pose.position.x;
                    double baseY = base_waypoints_.waypoints[ind].pose.pose.position.y;
                    double baseZ = base_waypoints_.waypoints[ind].pose.pose.position.z;
                    double nextX = base_waypoints_.waypoints[ind+1].pose.pose.position.x;
                    double nextY = base_waypoints_.waypoints[ind+1].pose.pose.position.y;
                    double nextZ = base_waypoints_.waypoints[ind+1].pose.pose.position.z;

                    tf::Vector3 base_vector(baseX, baseY, baseZ);
                    tf::Vector3 next_vector(nextX, nextY, baseZ);
                    tf::Vector3 current_vector;
                    if(ind ==  waypoints_base_index_) current_vector = tf::Vector3(current_pose_.pose.position.x, current_pose_.pose.position.y, current_pose_.pose.position.z);
                    else current_vector = base_vector;
                    double current_distance = tf::tfDistance(current_vector, base_vector);
                    double way_distance = tf::tfDistance(base_vector, next_vector);
                    double remaining_distance = way_distance - current_distance;   

                    if(remaining_distance > distance_v)
                    {
                        double proportion = (distance_v + current_distance) / way_distance;
                        geometry_msgs::PoseStamped cur_pose;
                        cur_pose.header.frame_id = "/map";
                        cur_pose.header.stamp = msg.header.stamp;
                        cur_pose.pose.position.x = baseX + (nextX - baseX) / proportion;
                        cur_pose.pose.position.y = baseY + (nextY - baseY) / proportion;
                        cur_pose.pose.position.z = baseZ + (nextZ - baseZ) / proportion;
                        cur_pose.pose.orientation.x = base_waypoints_.waypoints[ind].pose.pose.orientation.x;
                        cur_pose.pose.orientation.y = base_waypoints_.waypoints[ind].pose.pose.orientation.y;
                        cur_pose.pose.orientation.z = base_waypoints_.waypoints[ind].pose.pose.orientation.z;
                        cur_pose.pose.orientation.w = base_waypoints_.waypoints[ind].pose.pose.orientation.w;
                        geometry_msgs::TwistStamped cur_twist;
                        cur_twist.header.frame_id = "/velocity_base_link";
                        cur_twist.header.stamp = msg.header.stamp;
                        cur_twist.twist.linear.x = msg.velocity_mps;
                        cur_twist.twist.linear.y = 0;
                        cur_twist.twist.linear.z = 0;
                        cur_twist.twist.angular.x = 0;
                        cur_twist.twist.angular.y = 0;
                        cur_twist.twist.angular.z = 0;
                        current_pose_ = current_pose_;
                        current_velocity_ = cur_twist;
                        publishBaseLinkTF(ind, msg.header.stamp);
                        publishLocalizer(msg.header.stamp);
                        break;
                    }

                    distance_v -= remaining_distance;
                }
            }
            catch(tf::TransformException ex)
            {
                //error
            }
        }*/

        can502_ = msg;
    }

    void callbackOdom(const nav_msgs::Odometry &msg)
    {
        ros::Duration ros_time_diff = msg.header.stamp - odometry_.header.stamp;
        double time_diff = ros_time_diff.sec + ros_time_diff.nsec * 1E-9;

        double va = 0,  vb = time_diff;
        double vfa = odometry_.twist.twist.linear.x,  vfb = msg.twist.twist.linear.x;
        double vpm = (vfa + vfb) / 2.0;
        double distance_v = simpsons_rule(va, vfa, vb, vfb, vpm);

        std_msgs::Float64 msg_mileage;
        msg_mileage.data = distance_v;
        pub_mileage_.publish(msg_mileage);

        if(localizer_select_num_ == 2 && waypoints_base_index_ >= 0 && waypoints_base_index_ < base_waypoints_.waypoints.size()-1)
        {
            try
            {
                for(int ind=waypoints_base_index_; ind<base_waypoints_.waypoints.size()-1; ind++)
                {
                    double baseX = base_waypoints_.waypoints[ind].pose.pose.position.x;
                    double baseY = base_waypoints_.waypoints[ind].pose.pose.position.y;
                    double baseZ = base_waypoints_.waypoints[ind].pose.pose.position.z;
                    double nextX = base_waypoints_.waypoints[ind+1].pose.pose.position.x;
                    double nextY = base_waypoints_.waypoints[ind+1].pose.pose.position.y;
                    double nextZ = base_waypoints_.waypoints[ind+1].pose.pose.position.z;

                    tf::Vector3 base_vector(baseX, baseY, baseZ);
                    tf::Vector3 next_vector(nextX, nextY, baseZ);
                    tf::Vector3 current_vector;
                    if(ind ==  waypoints_base_index_) current_vector = tf::Vector3(current_pose_.pose.position.x, current_pose_.pose.position.y, current_pose_.pose.position.z);
                    else current_vector = base_vector;
                    double current_distance = tf::tfDistance(current_vector, base_vector);
                    double way_distance = tf::tfDistance(base_vector, next_vector);
                    double remaining_distance = way_distance - current_distance;   

                    if(remaining_distance > distance_v)
                    {
                        double proportion = (distance_v + current_distance) / way_distance;
                        geometry_msgs::PoseStamped cur_pose;
                        cur_pose.header.frame_id = "/map";
                        cur_pose.header.stamp = msg.header.stamp;
                        cur_pose.pose.position.x = baseX + (nextX - baseX) / proportion;
                        cur_pose.pose.position.y = baseY + (nextY - baseY) / proportion;
                        cur_pose.pose.position.z = baseZ + (nextZ - baseZ) / proportion;
                        cur_pose.pose.orientation.x = base_waypoints_.waypoints[ind].pose.pose.orientation.x;
                        cur_pose.pose.orientation.y = base_waypoints_.waypoints[ind].pose.pose.orientation.y;
                        cur_pose.pose.orientation.z = base_waypoints_.waypoints[ind].pose.pose.orientation.z;
                        cur_pose.pose.orientation.w = base_waypoints_.waypoints[ind].pose.pose.orientation.w;
                        geometry_msgs::TwistStamped cur_twist;
                        cur_twist.header.frame_id = "/velocity_base_link";
                        cur_twist.header.stamp = msg.header.stamp;
                        cur_twist.twist.linear.x = msg.twist.twist.linear.x;
                        cur_twist.twist.linear.y = 0;
                        cur_twist.twist.linear.z = 0;
                        cur_twist.twist.angular.x = 0;
                        cur_twist.twist.angular.y = 0;
                        cur_twist.twist.angular.z = msg.twist.twist.angular.z;

                        double lane_right_distance = mobileye_lane_.distance_to_right_lane;
                        double lane_left_distance  = -mobileye_lane_.distance_to_left_lane;
                        double lane_center = (mobileye_lane_.distance_to_left_lane + mobileye_lane_.distance_to_left_lane) / 2;
                        if(lane_right_distance < lane_left_distance)
                            cur_pose.pose.position.x += lane_left_distance - lane_center;
                        else
                            cur_pose.pose.position.x -= lane_right_distance - lane_center;

                        current_pose_ = cur_pose;
                        current_velocity_ = cur_twist;
                        publishBaseLinkTF(ind, msg.header.stamp);
                        publishLocalizer(msg.header.stamp);
                        break;
                    }

                    distance_v -= remaining_distance;
                }
            }
            catch(tf::TransformException ex)
            {
                //error
            }
        }

        odometry_ = msg;
    }

    void callbackLocalizerSelectNum(const std_msgs::Int32 &msg)
    {
        localizer_select_num_ = msg.data;
        if(localizer_select_num_ != 2)
        {
            calWaypintsFirstPosition(current_pose_.header.stamp);
            check_initTF_ = false;
            waypoints_base_index_ = -1;
        }
        else
        {
            calWaypintsFirstPosition(current_pose_.header.stamp);
            check_initTF_ = true;
        }
    }
public:
    VelocityLocalizer(ros::NodeHandle nh, ros::NodeHandle pnh)
        : nh_(nh)
        , pnh_(pnh)
        , localizer_select_num_(-1)
        , check_initTF_(false)
        , waypoints_base_index_(-1)
    {
        
        if (nh.getParam("tf_x", tf_vx_) == false)
        {
          std::cout << "tf_x is not set." << std::endl;
          return;
        }
        if (nh.getParam("tf_y", tf_vy_) == false)
        {
          std::cout << "tf_y is not set." << std::endl;
          return;
        }
        if (nh.getParam("tf_z", tf_vz_) == false)
        {
          std::cout << "tf_z is not set." << std::endl;
          return;
        }
        if (nh.getParam("tf_roll", tf_vroll_) == false)
        {
          std::cout << "tf_roll is not set." << std::endl;
          return;
        }
        if (nh.getParam("tf_pitch", tf_vpitch_) == false)
        {
          std::cout << "tf_pitch is not set." << std::endl;
          return;
        }
        if (nh.getParam("tf_yaw", tf_vyaw_) == false)
        {
          std::cout << "tf_yaw is not set." << std::endl;
          return;
        }

        pub_velocity_base_link_ = nh_.advertise<geometry_msgs::PoseStamped>("/velocity_base_link", 1);
        pub_velocity_twist_ = nh_.advertise<geometry_msgs::TwistStamped>("/velocity_estimate_twist", 1);
        pub_velocity_localizer_ = nh_.advertise<geometry_msgs::PoseStamped>("/velocity_localizer", 1);
        pub_mileage_ = nh_.advertise<std_msgs::Float64>("/mileage", 1);

        //sub_can502_ = nh.subscribe("/microbus/can_receive502", 1, &VelocityLocalizer::callbackCan502, this);
        //sub_imu_ = nh.subscribe("/imu_raw", 1, &VelocityLocalizer::callbackImu, this);
        //sub_base_waypoints_ = nh.subscribe("/base_waypoints", 1, &VelocityLocalizer::callbackBaseWaypoints, this);
        //sub_odom_ = nh.subscribe("/vehicle/odom", 1, &VelocityLocalizer::callbackOdom, this);
        //sub_mobileye_frame_ = nh.subscribe("/can_tx", 10 , &VelocityLocalizer::callbackMobileyeCan, this);

        sub_current_pose_ = new message_filters::Subscriber<geometry_msgs::PoseStamped>(nh_, "/current_pose", 1);
        sub_current_velocity_ = new message_filters::Subscriber<geometry_msgs::TwistStamped>(nh_, "/current_velocity", 1);
        sync_tp_ = new message_filters::Synchronizer<PoseTwistSync>(PoseTwistSync(SYNC_FRAMES), *sub_current_pose_, *sub_current_velocity_);
        sync_tp_->registerCallback(boost::bind(&VelocityLocalizer::PoseTwistCallback, this, _1, _2));
    }

    ~VelocityLocalizer()
    {
        delete sync_tp_;
        delete sub_current_pose_;
        delete sub_current_velocity_;
    }
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "velocity_localizer");
	ros::NodeHandle nh;
	ros::NodeHandle private_nh("~");

    VelocityLocalizer velocity_localizer(nh, private_nh);
    ros::spin();
    return 0;
}
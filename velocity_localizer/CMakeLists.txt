cmake_minimum_required(VERSION 2.8.3)
project(velocity_localizer)

#find_package(autoware_build_flags REQUIRED)
#find_package(autoware_msgs REQUIRED)
#find_package(autoware_config_msgs REQUIRED)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
  geometry_msgs
  sensor_msgs
  tf
  autoware_msgs
  autoware_can_msgs
  autoware_config_msgs
  mobileye_560_660_msgs
)

###################################
## catkin specific configuration ##
###################################
catkin_package(
        CATKIN_DEPENDS
        roscpp
        std_msgs
        sensor_msgs
        nav_msgs
        autoware_msgs
        autoware_can_msgs
        autoware_config_msgs
        mobileye_560_660_msgs
)

###########
## Build ##
###########

include_directories(include ${catkin_INCLUDE_DIRS})
# ${EIGEN3_INCLUDE_DIRS})

SET(CMAKE_CXX_FLAGS "-O2 -g -Wall -fpermissive ${CMAKE_CXX_FLAGS}")

add_executable(velocity_localizer nodes/velocity_localizer/velocity_localizer.cpp)
target_link_libraries(velocity_localizer ${catkin_LIBRARIES})
add_dependencies(velocity_localizer ${catkin_EXPORTED_TARGETS})

install(TARGETS
            velocity_localizer
        ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
        )

#install(DIRECTORY launch/
#        DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/launch
#        PATTERN ".svn" EXCLUDE)

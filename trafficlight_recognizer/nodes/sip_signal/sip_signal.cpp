#include <ros/ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>
#include <std_msgs/UInt8.h>
#include <geometry_msgs/PoseStamped.h>
#include <autoware_config_msgs/ConfigSipSignal.h>
#include <autoware_msgs/SipSignalInfo.h>
#include <autoware_msgs/WaypointParam.h>
#include <autoware_msgs/TrafficLight.h>
#include <autoware_msgs/Lane.h>
#include <limits.h>
#include "trafficlight_recognizer/traffic_light.h"

std::vector<std::string> split(const std::string &string, const char sep)
{
	std::vector<std::string> str_vec_ptr;
	std::string token;
	std::stringstream ss(string);

	while (getline(ss, token, sep))
		str_vec_ptr.push_back(token);

	return str_vec_ptr;
}

class SipSignal
{
private: 
	ros::NodeHandle nh_, p_nh_;
	ros::Subscriber sub_config_, sub_signal_info_, sub_local_waypoints_;
	ros::Publisher pub_signal_stat_, pub_signal_stat_string_, pub_sip_signal_error_, pub_takeover_flag_, pub_tmp_;
	ros::Publisher pub_sip_processing_can_lamp_info_, pub_alive_;

	autoware_config_msgs::ConfigSipSignal config_;
	autoware_msgs::WaypointParam waypoint_param_;
	autoware_msgs::SipSignalInfo sip_signal_info_;

	std::vector<unsigned char> sip_arrow_flag_list_;

	bool use_stop_acc_;
	double stop_acc_;
	double red_change_time_;
	bool lookahead_flag_;
	int prev_traffic_;
	unsigned char lane_signal_select_;

	double stop_acceleration(const autoware_msgs::Lane &lane, const int stopline_index)
	{
		if(red_change_time_ < 0) return DBL_MIN;
		if(lane.waypoints.size() < 1) return DBL_MIN;

		double red_sec_deltaT = 0, red_vel;
		int num1;
		for(num1=1; num1<=stopline_index; num1++)
		{
			geometry_msgs::Point pose = lane.waypoints[num1].pose.pose.position;
			geometry_msgs::Point pose_prev = lane.waypoints[num1-1].pose.pose.position;
			double deltaD = hypot(pose.x - pose_prev.x, pose.y - pose_prev.y);
			red_sec_deltaT += deltaD / lane.waypoints[num1].twist.twist.linear.x;
			if(red_change_time_ < red_sec_deltaT)
			{
				red_vel = lane.waypoints[num1].twist.twist.linear.x;
				break;
			}
		}
		if(num1 > stopline_index) return DBL_MIN;

		double red_distance = 0;
		for(int num2=num1; num2<=stopline_index; num2++)
		{
			geometry_msgs::Point pose = lane.waypoints[num2].pose.pose.position;
			geometry_msgs::Point pose_prev = lane.waypoints[num2-1].pose.pose.position;
			red_distance += hypot(pose.x - pose_prev.x, pose.y - pose_prev.y);
		}

		if(red_distance == 0) return DBL_MAX;
		//std::cout << "redD : " << red_distance << std::endl;
		double acc = red_vel * red_vel / (2 * red_distance);
		return acc;
	}

	void callbackLocalWaypoints(const autoware_msgs::Lane &lane)
	{
		//std::cout << lane.waypoints.size() << std::endl;
		//std::cout << "signal_select : " << +lane.signal_select << std::endl;
		double dt_sum = 0;
		geometry_msgs::Point pose_prev;
		for(int num=1; num<lane.waypoints.size(); num++)
		{
			const autoware_msgs::WaypointParam &param = lane.waypoints[num].waypoint_param;
			if(param.signal_stop_line > 0)
			{
				stop_acc_ = stop_acceleration(lane, num);
				use_stop_acc_ = true;
				waypoint_param_ = param;
				return;
			}

			geometry_msgs::Point pose = lane.waypoints[num].pose.pose.position;
			if(num > 1)
			{
				//end　judgment
				dt_sum += hypot(pose.x - pose_prev.x, pose.y - pose_prev.y);
				if(dt_sum >= config_.search_distance) break;
			}
			pose_prev = pose;
		}

		stop_acc_ = DBL_MIN;
		use_stop_acc_ = false;
		waypoint_param_.intersection_id = USHRT_MAX;
		waypoint_param_.routes_id = UCHAR_MAX;
		waypoint_param_.blue_arrow_flag = 0;
	}

	void callbackConfig(const autoware_config_msgs::ConfigSipSignal &msg)
	{
		config_ = msg;
	}

	void callbackSignalInfo(const autoware_msgs::SipSignalInfo &msg)
	{
		sip_signal_info_ = msg;
	}

	void publishSignal(int traffic_light, std::string str, bool takeover)
	{
		autoware_msgs::TrafficLight traffic_light_msg;
		std_msgs::String state_string_msg;
		traffic_light_msg.traffic_light = traffic_light;
		state_string_msg.data = str;
		pub_signal_stat_.publish(traffic_light_msg);
		pub_signal_stat_string_.publish(state_string_msg);

		std_msgs::Bool takeover_msg;
		takeover_msg.data = takeover;
		pub_takeover_flag_.publish(takeover_msg);

		prev_traffic_ = traffic_light;
	}

	void greenSignalTakeoverProcess()
	{
		if(lookahead_flag_ == false)
		{
			//std::cout << run_counter << "," << +car_lamp_info_list.car_lamp_id_ << "," <<+waypoint_param_.routes_id << "," << car_lamp_info_list.addres_number_ << std::endl;
			//std::cout << config_.takeover_acc << "," << stop_acc_ << std::endl;
			if(lane_signal_select_ == autoware_msgs::Lane::SIGNAL_SELECT_RED)
			{
				if(prev_traffic_ != TRAFFIC_LIGHT_GREEN) publishSignal(TRAFFIC_LIGHT_GREEN, std::string(TLR_GREEN_SIGNAL_STR), false);
				lookahead_flag_ = false;
			}
			//else if(stop_acc_ >= config_.takeover_acc + config_.time_plus)
			else if(stop_acc_ >= config_.takeover_acc)
			{
				if(prev_traffic_ != TRAFFIC_LIGHT_RED) publishSignal(TRAFFIC_LIGHT_RED, std::string(TLR_RED_SIGNAL_STR), true);
				lookahead_flag_ = true;
				system("aplay -D plughw:PCH /home/autoware/sakiyomi.wav &");
			}
			else
			{
				if(prev_traffic_ != TRAFFIC_LIGHT_GREEN) publishSignal(TRAFFIC_LIGHT_GREEN, std::string(TLR_GREEN_SIGNAL_STR), false);
				lookahead_flag_ = false;
			}
		}
		else
		{
			if(prev_traffic_ != TRAFFIC_LIGHT_RED) publishSignal(TRAFFIC_LIGHT_RED, std::string(TRAFFIC_LIGHT_RED), true);
			lookahead_flag_ = true;
		}
	}

	void greenSignalProcess()
	{
		if(prev_traffic_ != TRAFFIC_LIGHT_GREEN)
			publishSignal(TRAFFIC_LIGHT_GREEN, std::string(TLR_GREEN_SIGNAL_STR), false);
		lookahead_flag_ = false;
	}

	void redSignalProcess()
	{
		if(prev_traffic_ != TRAFFIC_LIGHT_RED)
			publishSignal(TRAFFIC_LIGHT_RED, std::string(TLR_RED_SIGNAL_STR), false);
		lookahead_flag_ = false;
	}

	void unkwonSignalProcess()
	{
		if(prev_traffic_ != TRAFFIC_LIGHT_UNKNOWN)
			publishSignal(TRAFFIC_LIGHT_UNKNOWN, std::string(TLR_UNKNOWN_SIGNAL_STR), false);
		lookahead_flag_ = false;
	}
public:
	SipSignal(ros::NodeHandle nh, ros::NodeHandle p_nh)
		: nh_(nh)
		, p_nh_(p_nh)
		, red_change_time_(-1)
		, lookahead_flag_(false)
		, prev_traffic_(TRAFFIC_LIGHT_UNKNOWN)
		, lane_signal_select_(autoware_msgs::Lane::SIGNAL_SELECT_UNKNOWN)
		, stop_acc_(DBL_MIN)
		, use_stop_acc_(false)
	{
		pub_signal_stat_ = nh_.advertise<autoware_msgs::TrafficLight>("/light_color", 10, true);
		pub_signal_stat_string_ = nh_.advertise<std_msgs::String>("/sound_player", 10);
		pub_sip_signal_error_ = nh_.advertise<std_msgs::String>("/sip/signal_error", 10);
		pub_takeover_flag_ = nh_.advertise<std_msgs::Bool>("/signal_takeover_flag", 10, true);
		pub_sip_processing_can_lamp_info_ = nh_.advertise<autoware_msgs::SipSignalCarLampInfo>("/sip/processing_can_lamp_info", 10);
		pub_alive_ = nh_.advertise<std_msgs::String>("/alive", 10);
		pub_tmp_ = nh_.advertise<std_msgs::String>("/sip/tmp2", 10);

		sub_config_ = nh_.subscribe("/config/sip_signal", 10, &SipSignal::callbackConfig, this);
		sub_signal_info_ = nh_.subscribe("/sip/signal_info", 10, &SipSignal::callbackSignalInfo, this);
		sub_local_waypoints_ = nh_.subscribe("/final_waypoints", 1, &SipSignal::callbackLocalWaypoints, this);

		config_.takeover_acc = 0.45;
		config_.search_distance = 100;

		for(int i=0; i<8; i++) sip_arrow_flag_list_.push_back((unsigned char)pow(2, i));

		publishSignal(TRAFFIC_LIGHT_UNKNOWN, std::string(TLR_UNKNOWN_SIGNAL_STR), false);
	}

	int run_counter = 0;
	void run()
	{
		std_msgs::String str_alive;
		str_alive.data = "/sip_signal";
		pub_alive_.publish(str_alive);

		std::cout << (int)use_stop_acc_ << std::endl;
		if(use_stop_acc_ == false)//停止線にぶつからない場合はUnkwonを送る
		{
			//std::cout << (int)use_stop_acc_ << std::endl;
			unkwonSignalProcess();
			red_change_time_ = -1;
			return;
		}

		if(sip_signal_info_.first_info.intersection_id_ == waypoint_param_.intersection_id)
		{
			bool publish_flag = false;
			for(autoware_msgs::SipSignalServiceInfo service_info : sip_signal_info_.service_info)
			{
				if(service_info.routes_id_ == waypoint_param_.routes_id)//waypointとの方路IDチェック
				{
					for(short ptr : service_info.car_lamp_info_ptr_)//色情報データのアドレス取得
					{
						if(ptr == -1) continue;

						for(autoware_msgs::SipSignalCarLampInfoList car_lamp_info_list : sip_signal_info_.car_lamp_info_list)
						{
							std::cout << car_lamp_info_list.addres_number_ << "," << ptr << std::endl;
							if(car_lamp_info_list.addres_number_ == ptr+13) //何故か13たさないと合わない
							{
								//std::cout << ptr + 13 << std::endl;
								//std::cout << stop_acc_ + config_.time_plus << "," << config_.takeover_acc << std::endl;
								//最初の信号情報が現在の信号状態
								int first_lamp_ind = 0;
								autoware_msgs::SipSignalCarLampInfo lamp_info = car_lamp_info_list.car_lamp_info_[first_lamp_ind];
								pub_sip_processing_can_lamp_info_.publish(lamp_info);

								//std::cout << +lamp_info.round_signal_color_view_ << std::endl;
								switch(lamp_info.round_signal_color_view_)
								{
								case autoware_msgs::SipSignalCarLampInfo::ROUND_BLUE://青
									if(car_lamp_info_list.car_lamp_info_.size() < 2)//次の色情報がない
									{
										//std::cout << "a" << std::endl;
										//greenSignalTakeoverProcess();
										//red_change_time_ = lamp_info.min_remain_sec_ / 10.0 + next_lamp_info.min_remain_sec_;//黄色分の時間を追加
										greenSignalProcess();
										red_change_time_ = -1;
										publish_flag = true;
									}
									else
									{
										autoware_msgs::SipSignalCarLampInfo next_lamp_info = car_lamp_info_list.car_lamp_info_[first_lamp_ind+1];
										if(next_lamp_info.round_signal_color_view_ == autoware_msgs::SipSignalCarLampInfo::ROUND_BLUE)//次の色情報が青信号
										{
											//std::cout << "b" << std::endl;
											greenSignalProcess();
											red_change_time_ = -1;
											publish_flag = true;
										}
										else//矢印チェック
										{
											unsigned char waypoint_arrow = sip_arrow_flag_list_[waypoint_param_.blue_arrow_flag];
											if(next_lamp_info.blue_arrow_signal_color_view_ & waypoint_arrow)//進む方向である
											{
												//std::cout << "c" << std::endl;
												greenSignalProcess();
												red_change_time_ = -1;
												publish_flag = true;
											}
											else//進む方向ではない
											{
												//std::cout << "d" << std::endl;
												//greenSignalTakeoverProcess();
												//red_change_time_ = lamp_info.min_remain_sec_ / 10.0 + next_lamp_info.min_remain_sec_;//黄色分の時間を追加
												greenSignalProcess();
												red_change_time_ = -1;
												publish_flag = true;
											}
										}
									}
									
									break;
								case autoware_msgs::SipSignalCarLampInfo::ROUND_YELLOW://黄
									redSignalProcess();
									red_change_time_ = -1;
									publish_flag = true;
									break;
								case autoware_msgs::SipSignalCarLampInfo::ROUND_RED://赤
									std::cout << "red" << std::endl;
									if(lamp_info.blue_arrow_signal_color_view_ == 0)//矢印なし
									{
										//std::cout << "a" << std::endl;
										redSignalProcess();
										red_change_time_ = -1;
										publish_flag = true;
									}
									else//矢印あり
									{
										unsigned char waypoint_arrow = sip_arrow_flag_list_[waypoint_param_.blue_arrow_flag-1];
										std::cout << run_counter << "," << +waypoint_arrow << "," << +lamp_info.blue_arrow_signal_color_view_ << std::endl;
										if(lamp_info.blue_arrow_signal_color_view_ & waypoint_arrow)//進む方向である
										{
											if(car_lamp_info_list.car_lamp_info_.size() < 2)//次の色情報がない
											{
												//std::cout << "b" << std::endl;
												greenSignalProcess();
												red_change_time_ = -1;
												publish_flag = true;
											}
											else//次の色情報あり(先読み処理)
											{
												autoware_msgs::SipSignalCarLampInfo next_lamp_info = car_lamp_info_list.car_lamp_info_[1];
												switch(next_lamp_info.round_signal_color_view_)
												{
												case autoware_msgs::SipSignalCarLampInfo::ROUND_BLUE:
													//std::cout << "c" << std::endl;
													greenSignalProcess();
													red_change_time_ = -1;
													publish_flag = true;
													break;
												case autoware_msgs::SipSignalCarLampInfo::ROUND_YELLOW:
												case autoware_msgs::SipSignalCarLampInfo::ROUND_RED:
													//std::cout << "d" << std::endl;
													//greenSignalTakeoverProcess();
													//red_change_time_ = lamp_info.min_remain_sec_ / 10.0 + next_lamp_info.min_remain_sec_;//黄色分の時間を追加
													greenSignalProcess();
													red_change_time_ = -1;
													publish_flag = true;
													break;
												}
											}
										}
										else//進む方向ではない
										{
											//std::cout << "e" << std::endl;
											redSignalProcess();
											red_change_time_ = -1;
											publish_flag = true;
										}
									}
									
									break;
								defalut:
									unkwonSignalProcess();
									red_change_time_ = -1;
									publish_flag = true;
								}

								char buf[100];
								sprintf(buf, "%d,%d,%d,%d,%d,%d,%d", run_counter, (int)publish_flag, car_lamp_info_list.car_lamp_id_, car_lamp_info_list.addres_number_, lamp_info.round_signal_color_view_, lamp_info.min_remain_sec_, lamp_info.max_remain_sec_);
								std_msgs::String str_tmp;
								str_tmp.data = buf;
								pub_tmp_.publish(str_tmp);
								break;
							}
						}
					
						if(publish_flag == true) break;
					}

					break;
				}
			}

			std_msgs::String error_str;
			//信号情報が取得できない場合
			if(publish_flag == false)
			{
				if(prev_traffic_ != TRAFFIC_LIGHT_UNKNOWN)
					unkwonSignalProcess();
					//publishSignal(TRAFFIC_LIGHT_UNKNOWN, std::string(TLR_UNKNOWN_SIGNAL_STR), false);
				red_change_time_ = -1;

				error_str.data = "not sip signal";
				pub_sip_signal_error_.publish(error_str);
				run_counter++;
				return;
			}
			else
			{
				error_str.data = "";
				pub_sip_signal_error_.publish(error_str);
				run_counter++;
				return;
			}
		}

		run_counter++;
	}
};

int main(int argc, char** argv)
{
	ros::init(argc, argv, "sip_signal");
	ros::NodeHandle nh;
	ros::NodeHandle private_nh("~");

	SipSignal ss(nh, private_nh);
	ros::Rate rate(30);
	while(ros::ok())
	{
		ros::spinOnce();
		ss.run();
		rate.sleep();
	}
	return 0;
}
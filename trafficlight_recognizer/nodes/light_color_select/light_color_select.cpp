#include <ros/ros.h>
#include <autoware_msgs/TrafficLight.h>
#include <std_msgs/String.h>
#include <autoware_config_msgs/ConfigLightColorSelect.h>
#include "trafficlight_recognizer/traffic_light.h"
#include <autoware_msgs/Lane.h>
#include <std_msgs/Bool.h>
#include "trafficlight_recognizer/traffic_light.h"

class LightColorSelect
{
private:
    ros::NodeHandle nh_, private_nh_;

    ros::Subscriber sub_config_, sub_light_color_1_, sub_light_color_2_, sub_light_color_3_, sub_light_color_4_;
    ros::Subscriber sub_waypoints_;
    ros::Publisher pub_light_color_, pub_light_string_, pub_signal_acc_over_;

    static const int topic_count_ = 4;
    autoware_msgs::TrafficLight light_[topic_count_];

    autoware_config_msgs::ConfigLightColorSelect config_;
    autoware_msgs::Lane lane_;
    int32_t prev_signal_;

    void callbackConfig(const autoware_config_msgs::ConfigLightColorSelect &msg)
    {
        config_ = msg;
    }

    void callbackLightColor1(const autoware_msgs::TrafficLight &msg)
    {
        light_[0] = msg;
    }

    void callbackLightColor2(const autoware_msgs::TrafficLight &msg)
    {
        light_[1] = msg;
    }

    void callbackLightColor3(const autoware_msgs::TrafficLight &msg)
    {
        light_[2] = msg;   
    }

    void callbackLightColor4(const autoware_msgs::TrafficLight &msg)
    {
        light_[3] = msg;
    }

    void callbackWaypoints(const autoware_msgs::Lane &msg)
    {
        lane_ = msg;
    }

    double stop_acceleration(const int stopline_index)
    {
        if(lane_.waypoints.size() < 1) return DBL_MIN;

        double red_vel = lane_.waypoints[1].twist.twist.linear.x;
        double red_distance = 0;

        for(int num2=1; num2<=stopline_index; num2++)
        {
            geometry_msgs::Point pose = lane_.waypoints[num2].pose.pose.position;
            geometry_msgs::Point pose_prev = lane_.waypoints[num2-1].pose.pose.position;
            red_distance += hypot(pose.x - pose_prev.x, pose.y - pose_prev.y);
        }

        if(red_distance == 0) return DBL_MAX;
        //std::cout << "redD : " << red_distance << std::endl;
        double acc = red_vel * red_vel / (2 * red_distance);
        return acc;
    }

    void publish_takeover_flag()
    {
        double stop_acc = DBL_MIN;
        double dt_sum = 0;
        geometry_msgs::Point pose_prev;

        for(int num=1; num<lane_.waypoints.size(); num++)
        {
            if(lane_.waypoints[num].waypoint_param.signal_stop_line <= autoware_msgs::WaypointParam::SIGNAL_STOP_LINE_NONE)
            {
                geometry_msgs::Point pose = lane_.waypoints[num].pose.pose.position;
                if(num > 1)
                {
                    //end　judgment
                    dt_sum += hypot(pose.x - pose_prev.x, pose.y - pose_prev.y);
                    if(dt_sum >= config_.search_distance) break;
                }
                pose_prev = pose;
            }
            else
            {
                stop_acc = stop_acceleration(num);
                break;
            }
        }

        std_msgs::Bool over_ret;
        over_ret.data = (stop_acc >= config_.takeover_acc) ? true : false;
        pub_signal_acc_over_.publish(over_ret);
    }
public:
    LightColorSelect(ros::NodeHandle nh, ros::NodeHandle p_nh)
        : nh_(nh)
        , private_nh_(p_nh)
        , prev_signal_(TRAFFIC_LIGHT_UNKNOWN)
    {
        std::string light_topic;

        sub_config_ = nh_.subscribe("/config/light_color_select", 1, &LightColorSelect::callbackConfig, this);

        private_nh_.param<std::string>("topic1", light_topic, "");
        if(light_topic != "")
        {
            sub_light_color_1_ = nh_.subscribe(light_topic, 1, &LightColorSelect::callbackLightColor1, this);
        }

        private_nh_.param<std::string>("topic2", light_topic, "");
        if(light_topic != "")
        {
            sub_light_color_2_ = nh_.subscribe(light_topic, 1, &LightColorSelect::callbackLightColor2, this);
        }

        private_nh_.param<std::string>("topic3", light_topic, "");
        if(light_topic != "")
        {
            sub_light_color_3_ = nh_.subscribe(light_topic, 1, &LightColorSelect::callbackLightColor3, this);
        }

        private_nh_.param<std::string>("topic4", light_topic, "");
        if(light_topic != "")
        {
            sub_light_color_4_ = nh_.subscribe(light_topic, 1, &LightColorSelect::callbackLightColor4, this);
        }
        sub_waypoints_ = nh_.subscribe("final_waypoints", 1, &LightColorSelect::callbackWaypoints, this);

        pub_light_color_ = nh_.advertise<autoware_msgs::TrafficLight>("/light_color", 1, true);
        pub_light_string_ = nh_.advertise<std_msgs::String>("/sound_player", 1);
        pub_signal_acc_over_ = nh_.advertise<std_msgs::Bool>("/signal_acc_over", 1);

        config_.priority1 = 1;  config_.priority2 = 2;  config_.priority3 = 3;  config_.priority4 = 4;

        for(int i=0; i<topic_count_; i++) light_[i].traffic_light = TRAFFIC_LIGHT_UNKNOWN;
    }

    void run()
    {
        autoware_msgs::TrafficLight ret;
        ret.traffic_light = TRAFFIC_LIGHT_UNKNOWN;
        if(light_[config_.priority1-1].traffic_light != TRAFFIC_LIGHT_UNKNOWN) ret = light_[config_.priority1-1];
        else if(light_[config_.priority2-1].traffic_light != TRAFFIC_LIGHT_UNKNOWN) ret = light_[config_.priority2-1];
        else if(light_[config_.priority3-1].traffic_light != TRAFFIC_LIGHT_UNKNOWN) ret = light_[config_.priority3-1];
        else if(light_[config_.priority4-1].traffic_light != TRAFFIC_LIGHT_UNKNOWN) ret = light_[config_.priority4-1];

        std_msgs::String str;
        switch(ret.traffic_light)
        {
            case TRAFFIC_LIGHT_GREEN:
                if(prev_signal_ != TRAFFIC_LIGHT_GREEN)
                {
                    str.data = std::string(TLR_GREEN_SIGNAL_STR);
                    prev_signal_ = TRAFFIC_LIGHT_GREEN;
                    pub_light_color_.publish(ret);
                    pub_light_string_.publish(str);
                }
                break;
            case TRAFFIC_LIGHT_RED:
                if(prev_signal_ != TRAFFIC_LIGHT_RED)
                {
                    str.data = std::string(TLR_RED_SIGNAL_STR);
                    prev_signal_ = TRAFFIC_LIGHT_RED;
                    pub_light_color_.publish(ret);
                    pub_light_string_.publish(str);
                    publish_takeover_flag();
                }
                break;
            default:
                if(prev_signal_ != TRAFFIC_LIGHT_UNKNOWN)
                {
                    str.data = std::string(TLR_UNKNOWN_SIGNAL_STR);
                    prev_signal_ = TRAFFIC_LIGHT_UNKNOWN;
                    pub_light_color_.publish(ret);
                    pub_light_string_.publish(str);
                    publish_takeover_flag();
                }
                break;
        }
    }

};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "light_color_select");
	ros::NodeHandle nh;
	ros::NodeHandle private_nh("~");

    LightColorSelect lcs(nh, private_nh);

    ros::Rate rate(60);
    while(ros::ok())
    {
        ros::spinOnce();
        lcs.run();
        rate.sleep();
    }
    return 0;
}
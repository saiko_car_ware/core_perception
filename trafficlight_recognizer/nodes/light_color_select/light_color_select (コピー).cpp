#include <ros/ros.h>
#include <autoware_msgs/TrafficLight.h>
#include <autoware_config_msgs/ConfigLightColorSelect.h>
#include "trafficlight_recognizer/traffic_light.h"

class LightColorSelect
{
private:
    ros::NodeHandle nh_, private_nh_;

    ros::Subscriber sub_config_, sub_light_color_1_, sub_light_color_2_, sub_light_color_3_, sub_light_color_4_;
    ros::Publisher pub_light_color_;

    static const int topic_count_ = 4;
    autoware_msgs::TrafficLight light_[topic_count_];

    autoware_config_msgs::ConfigLightColorSelect config_;

    void callbackConfig(const autoware_config_msgs::ConfigLightColorSelect &msg)
    {
        config_ = msg;
    }

    void callbackLightColor1(const autoware_msgs::TrafficLight &msg)
    {
        light_[0] = msg;
    }

    void callbackLightColor2(const autoware_msgs::TrafficLight &msg)
    {
        light_[1] = msg;
    }

    void callbackLightColor3(const autoware_msgs::TrafficLight &msg)
    {
        light_[2] = msg;   
    }

    void callbackLightColor4(const autoware_msgs::TrafficLight &msg)
    {
        light_[3] = msg;
    }
public:
    LightColorSelect(ros::NodeHandle nh, ros::NodeHandle p_nh)
        : nh_(nh)
        , private_nh_(p_nh)
    {
        std::string light_topic;

        sub_config_ = nh_.subscribe("/config/light_color_select", 1, &LightColorSelect::callbackConfig, this);

        private_nh_.param<std::string>("topic1", light_topic, "");
        if(light_topic != "")
        {
            sub_light_color_1_ = nh_.subscribe(light_topic, 1, &LightColorSelect::callbackLightColor1, this);
        }

        private_nh_.param<std::string>("topic2", light_topic, "");
        if(light_topic != "")
        {
            sub_light_color_2_ = nh_.subscribe(light_topic, 1, &LightColorSelect::callbackLightColor2, this);
        }

        private_nh_.param<std::string>("topic3", light_topic, "");
        if(light_topic != "")
        {
            sub_light_color_3_ = nh_.subscribe(light_topic, 1, &LightColorSelect::callbackLightColor3, this);
        }

        private_nh_.param<std::string>("topic4", light_topic, "");
        if(light_topic != "")
        {
            sub_light_color_4_ = nh_.subscribe(light_topic, 1, &LightColorSelect::callbackLightColor4, this);
        }

        pub_light_color_ = nh_.advertise<autoware_msgs::TrafficLight>("/light_color", 1);

        config_.priority1 = 1;  config_.priority2 = 2;  config_.priority3 = 3;  config_.priority4 = 4;
    }

    void run()
    {
        autoware_msgs::TrafficLight ret;
        ret.traffic_light = TRAFFIC_LIGHT_UNKNOWN;
        if(light_[config_.priority1-1].traffic_light != TRAFFIC_LIGHT_UNKNOWN) ret = light_[config_.priority1-1];
        else if(light_[config_.priority2-1].traffic_light != TRAFFIC_LIGHT_UNKNOWN) ret = light_[config_.priority2-1];
        else if(light_[config_.priority3-1].traffic_light != TRAFFIC_LIGHT_UNKNOWN) ret = light_[config_.priority3-1];
        else if(light_[config_.priority4-1].traffic_light != TRAFFIC_LIGHT_UNKNOWN) ret = light_[config_.priority4-1];
        pub_light_color_.publish(ret);
    }
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "light_color_select");
	ros::NodeHandle nh;
	ros::NodeHandle private_nh("~");

    LightColorSelect lcs(nh, private_nh);

    ros::Rate rate(30);
    while(ros::ok())
    {
        ros::spinOnce();
        lcs.run();
        rate.sleep();
    }
    return 0;
}
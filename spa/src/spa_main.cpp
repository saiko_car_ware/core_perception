#include <ros/ros.h>
#include <autoware_config_msgs/ConfigSPA.h>
#include <autoware_msgs/SolarDirection.h>
#include "spa_utils.h"
#include <std_msgs/String.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
//#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PoseStamped.h>

class SPA
{
private:
    ros::NodeHandle nh_, private_nh_;
    ros::Subscriber sub_config_, sub_sentence_, sub_current_pose_;
    ros::Publisher pub_solar_direction_;

    tf::TransformListener listener_;

    autoware_config_msgs::ConfigSPA config_;
    spa_data spadata_;
    spa_camparam cam_param_;


    void init(const double timezone, const double pressure, const double temperature, const double atmos_refract,
              const double focul_length, const double sensor_size_x, const double sensor_size_y,
              const double image_size_x, const double image_size_y)
    {
        spa_init_spa_data(&spadata_, timezone, pressure, temperature, atmos_refract);
        spa_init_spa_camparam(&cam_param_, focul_length, sensor_size_x, sensor_size_y, image_size_x, image_size_y);
    }

    void callbackConfig(const autoware_config_msgs::ConfigSPA &msg)
    {
        config_ = msg;
        init(msg.timezone, msg.pressure, msg.temperature, msg.atmos_refract,
             msg.focul_length, msg.sensor_size_x, msg.sensor_size_y, msg.image_size_x, msg.image_size_y);
    }

    void callbackSntence(const std_msgs::String &msg)
    {
        char buf[SPA_MAX_BUFLEN];
        strcpy(buf, msg.data.c_str());
        if ( strlen ( buf ) >= 1 )
		buf[strlen(buf)-1] = '\0';

        /*tf::StampedTransform camera_to_baselink;
		try{
			listener_.lookupTransform("/base_link", "/camera", ros::Time(0), camera_to_baselink);
		}
		catch (tf::TransformException ex){
			//ROS_ERROR("%s",ex.what());
			//return;
		}*/

        unsigned long frame;
		int result = spa_send_data(buf, &frame, &spadata_, &cam_param_);

        autoware_msgs::SolarDirection solar_msg;
        solar_msg.header.frame_id = "map";
        solar_msg.header.stamp = ros::Time::now();
        solar_msg.zenith = spadata_.zenith;
        solar_msg.azimuth = spadata_.azimuth;
        solar_msg.quaternion = tf::createQuaternionMsgFromRollPitchYaw(spadata_.zenith*M_PI/180.0, 0, spadata_.azimuth*M_PI/180.0);
        if ( result == 1 )
        {
            printf( "SolPos;%d,%d; ", cam_param_.posX, cam_param_.posY );
            printf( "Solar_zenith;%g; ", spadata_.zenith );
            printf( "Solar_azimuth;%g; ", spadata_.azimuth );
            spa_print_spatime ( &spadata_ );
            solar_msg.direction_flag = true;
        }
        else solar_msg.direction_flag = false;
        pub_solar_direction_.publish(solar_msg);
    }
public:
    SPA(const ros::NodeHandle nh, const ros::NodeHandle pnh)
        : nh_(nh)
        , private_nh_(pnh)
    {
        init(0, 1013.25, 20, 0.5667, 16.0, 1936*5.86e-3, 1216*5.86e-3, 960, 600);

        sub_config_ = nh_.subscribe("/config/spa", 1 , &SPA::callbackConfig, this);
        sub_sentence_ = nh_.subscribe("/nmea_sentence", 1 , &SPA::callbackSntence, this);
        pub_solar_direction_ = nh_.advertise<autoware_msgs::SolarDirection>("/solar_direction", 1);
    }
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "spa");
	ros::NodeHandle nh;
	ros::NodeHandle private_nh("~");

    SPA spa(nh, private_nh);

    ros::spin();
    return 0;
}
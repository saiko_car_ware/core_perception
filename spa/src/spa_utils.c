
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "spa.h"
#include "spa_utils.h"

// #define DEBUG

/* acceptable data TAG */
char datatype_identifier [][16] = { "#BESTPOSA","#INSATTXA","#INSPVAXA","$GPGGA","$GPGST","$GPHDT","$GPVTG","$PASHR","%INSPVASA","%INSSTDEVSA", "#TIMEA"};

typedef enum _dataType 
{
 BESTPOSA = 0,
 INSATTXA = 1,
 INSPVAXA = 2,
 GPGGA = 3, 
 GPGST = 4,
 GPHDT = 5,
 GPVTG = 6,
 PASHR = 7,
 INSPVASA = 8,
 INSSTDEVSA = 9,
 TIMEA = 10,
 NONE = 999
}dataType;


/* ----------------------------------------------------------------
 *
 * static functions ( they intend to be called within the file )
 * 
 * ---------------------------------------------------------------- */

static void self_dgemm ( int M, int N, int K, double *a, double *b, double *c )
{
	int m, n, k;

	for ( m = 0; m < M; m++ )
		for ( k = 0; k < K; k++ ){
			c[m*K + k] = 0;
			for ( n = 0; n < N; n++ )
				c[m*K+k] += a[m*N+n] * b[n*K+k];
		}
	
}

static void debug_dgemm ( int M, int N, double *a )
{
	int m, n;
	for ( m = 0; m < M; m++ ){
		for ( n = 0; n < N; n++ )
			printf( "%f ", a[m*N+n] );
		printf( "\n" );
	}
}

static void rotation_3d ( double xyz[3],
													double angle[3] )
{
	double rotation_mat[9];
	double temp1[9], temp2[9];
	double rangle[3];
	double results[3];

	int i ;

	for ( i = 0; i < 3; i++ )
		rangle[i] = deg2rad(angle[i]);

	// Azimuth rotation
	temp1[0] = cos(rangle[2]); temp1[1] = sin(rangle[2]); temp1[2] = 0;
	temp1[3] =-sin(rangle[2]); temp1[4] = cos(rangle[2]); temp1[5] = 0;
	temp1[6] = 0; temp1[7] = 0; temp1[8] = 1;

	// Roll rotation
	temp2[0] = 1; temp2[1] = 0; temp2[2] = 0;
	temp2[3] = 0; temp2[4] = cos(rangle[0]); temp2[5] = sin(rangle[0]);
	temp2[6] = 0; temp2[7] = -sin(rangle[0]); temp2[8] = cos(rangle[0]);

	self_dgemm ( 3, 3, 3, temp2, temp1, rotation_mat );
	memcpy ( temp1, rotation_mat, 9 * sizeof(double) );
	
	// Pitch rotation
	temp2[0] = cos(rangle[1]); temp2[1] = 0; temp2[2] = -sin(rangle[1]);
	temp2[3] = 0; temp2[4] = 1; temp2[5] = 0;
	temp2[6] = sin(rangle[1]); temp2[7] = 0; temp2[8] = cos(rangle[1]);
	self_dgemm ( 3, 3, 3, temp2, temp1, rotation_mat );

	self_dgemm ( 3, 3, 1, rotation_mat, xyz, results );

	memcpy ( xyz, results, 3 * sizeof(double) );
}

static int getType ( void * buf_ )
{
	char *buf;
	int i;
	buf = (char *) buf_;
	
	for ( i = 0; i < 11; i++ ) 
		if ( strstr ( buf, datatype_identifier[i] ) )
			return i;
		
	return NONE;
}

static void get_sun_position ( spa_data *spa, spa_camparam *cam_param )
{
	double azimuth, elevation;
	double deltaX, deltaY;
	int offsetX, offsetY;
	
	azimuth = spa->azimuth - cam_param->azimuth;
	elevation = (90 - spa->zenith) - cam_param->pitch;

	deltaX = cam_param->f * tan( azimuth * M_PI / 180 ) / cam_param->sensor_width; /* in ratio */
	deltaY = -cam_param->f * tan( elevation * M_PI / 180 ) / cam_param->sensor_height; /* in ratio */

	cam_param->posX = (deltaX * cam_param->imgW) +  cam_param->imgW/2;
	cam_param->posY = (deltaY * cam_param->imgH) +  cam_param->imgH/2;
}



/* ===========================================
 *
 *	spa_init_spa_data -- initialize spa_data structure
 *
 * =========================================== */
int spa_init_spa_data ( spa_data * spa,
												
												double timezone, 
												// Observer time zone (negative west of Greenwich)
												// valid range: -18   to   18 hours,   error code: 8
												
												double pressure,
												// Annual average local pressure [millibars]
												// valid range:    0 to 5000 millibars,       error code: 12

												double temperature,
												// Annual average local temperature [degrees Celsius]
												// valid range: -273 to 6000 degrees Celsius, error code; 13
												
												double atmos_refract
												// Atmospheric refraction at sunrise and sunset (0.5667 deg is typical)
												// valid range: -5   to   5 degrees, error code: 16
												)
{
	memset( spa, 0, sizeof(spa_data) );

	spa->timezone = timezone;
	spa->pressure = pressure;
	spa->temperature = temperature;
	spa->atmos_refract = atmos_refract;
	spa->function      = SPA_ALL;
		
	return 0;
}


/* ===========================================
 *
 *	spa_init_spa_camparam -- set camera capturing parameters
 *
 * =========================================== */
int spa_init_spa_camparam ( spa_camparam * cam,
														double f, /* focul length (mm) */
														double sensor_width,  /* (mm) */
														double sensor_height, /* (mm) */
														int  imgW, /* image width (pix) */
														int  imgH /* image height (pix) */ )
{

	memset( cam, 0, sizeof(spa_camparam) );
	
	cam -> f = f;
	cam -> sensor_width = sensor_width;
	cam -> sensor_height = sensor_height;
	cam -> imgW = imgW;
	cam -> imgH = imgH;

	cam -> vangle_width = atan2 ( cam->f, cam->sensor_width/2 ) * 2; /* in rad */
	cam -> vangle_height = atan2 ( cam->f, cam->sensor_height/2 ) * 2; /* in rad */
	
	return 0;
}
										


/* ====================================
 *
 *	spa_send_data -- sending sensor data to obtain solar position
 *
 * ==================================== */
int spa_send_data ( void * buf,
										unsigned long *frames,
										spa_data * spa,
										spa_camparam * cam_param )
{
	char * subbuf;
	char tmp[SPA_MAX_TMPBUFLEN][SPA_MAX_MESSAGELEN];
	double tmp_dbl[SPA_MAX_TMPBUFLEN];
	double n_plane [3];
	int i;
	int result;
	int ret_value = 0;

	subbuf = NULL;
	subbuf = strstr ( (char *)buf, ";" );
	
	switch ( getType ( buf ) )
		{
		case BESTPOSA:
			
			sscanf ( subbuf, "%[^,],%[^,],%lg,%lg,%lg,%s",
							 tmp[0], tmp[1], &(spa->latitude), &(spa->longitude), &(spa->elevation), tmp[2] );
			
#ifdef DEBUG
			printf( "\tLatitude:      %.6g (degrees)\n", spa->latitude );
			printf( "\tLongitude:     %.6g (degrees)\n", spa->longitude );
			printf( "\tElevation:     %.6g (meters)\n", spa->elevation );
#endif

			break;

		case INSATTXA:
			//			printf ( "INSATTXA  %s\n", subbuf );
			sscanf ( subbuf, "%[^,],%[^,],%lg,%lg,%lg,%s",
							 tmp[0], tmp[1], &(tmp_dbl[0]), &(tmp_dbl[1]), &(tmp_dbl[2]), tmp[2] );
			cam_param->roll = tmp_dbl[0];
			cam_param->pitch = tmp_dbl[1];
			cam_param->azimuth= tmp_dbl[2];

#ifdef DEBUG
			printf( "\tRoll:          %.6g (degrees)\n", tmp_dbl[0] );
			printf( "\tPitch:         %.6g (degrees)\n", tmp_dbl[1] );
			printf( "\tAzimuth:       %.6g (degrees)\n", tmp_dbl[2] );
#endif

			n_plane[0] = 0; n_plane[1] = 0; n_plane[2] = 1;
			rotation_3d ( n_plane, tmp_dbl );

			spa->slope = rad2deg ( acos( n_plane[2] ) );
			spa->azm_rotation = rad2deg ( atan2( n_plane[0], n_plane[2] ) );
			if ( spa->azm_rotation > 0 ) 
				spa->azm_rotation -= 180;
			else
				spa->azm_rotation += 180;

#ifdef DEBUG
			printf( "\tslope:         %.6g  (degrees)\n", spa->slope );
			printf( "\tazm_rotation:  %.6g  (degrees)\n", spa->azm_rotation );
#endif			

			break;

		case INSPVAXA:
			//			printf ( "INSPVAXA %s\n", subbuf );
			break;

		case GPGGA:
			//			printf ( "GPGGA %s\n", subbuf );
			break;

		case GPGST:
			//			printf ( "GPGST %s\n", subbuf );		
			break;

		case GPHDT:
			//			printf ( "GPHDT %s\n", subbuf );			
			break;

		case GPVTG:
			//			printf ( "GPVTG %s\n", subbuf );		
			break;

		case PASHR:
			//			printf ( "PASHR %s\n", subbuf );		
			break;

		case INSPVASA:
			//			printf ( "INSPVASA %s\n", subbuf );			
			break;

		case INSSTDEVSA:
			//			printf ( "INSSTDEVSA %s\n", subbuf );			
			break;

		case TIMEA:

			sscanf ( subbuf, "%[^,],%lg,%lg,%lg,%d,%d,%d,%d,%d,%lg,%s",
							 tmp[0], tmp_dbl, tmp_dbl+1, tmp_dbl+2, 
							 &(spa->year), &(spa->month), &(spa->day), &(spa->hour), &(spa->minute), &(spa->second), tmp[1] );
			spa->second /= 1000;

			result = spa_calculate(spa);
			
#ifdef DEBUG
			if (result == 0)  //check for SPA errors
				{
					//display the results inside the SPA structure
					
					//						printf("Julian Day:    %.6f\n",spa->jd);
					//						printf("L:             %.6e degrees\n",spa->l);
					//						printf("B:             %.6e degrees\n",spa->b);
					//						printf("R:             %.6f AU\n",spa->r);
					//						printf("H:             %.6f degrees\n",spa->h);
					//						printf("Delta Psi:     %.6e degrees\n",spa->del_psi);
					//						printf("Delta Epsilon: %.6e degrees\n",spa->del_epsilon);
					//						printf("Epsilon:       %.6f degrees\n",spa->epsilon);
					printf ( "\tspa_time =  %4d/%02d/%02d %02d:%02d:%02f \n", 
									 spa->year,spa->month,spa->day, spa->hour, spa->minute, spa->second );
					
					printf("\t==================\n");
					printf ( "\t Calculated Solar Position \n" );						
					printf("\t------------------\n");
					printf("\tSolar Zenith:    %.6f degrees\n",spa->zenith);
					printf("\tSolar Elevation: %.6f degrees\n",90-spa->zenith);
					printf("\tSolar Azimuth:   %.6f degrees\n",spa->azimuth);
					//						printf("Incidence:     %.6f degrees\n",spa->incidence);
					
				} else printf("SPA Error Code: %d\n", result);
#endif
			
			if ( result == 0 )
				get_sun_position ( spa, cam_param );

#ifdef DEBUG				
			printf( "POS:%d,%d:%s\n", cam_param -> posX, cam_param -> posY, buf );
#endif

			ret_value = 1;
			(*frames)++;
			break;
			
		default:

			break;
		}
	return ret_value;
}

/* ====================================
 *
 *	spa_print_spatime -- print the current SPA time 
 *
 * ==================================== */
int spa_print_spatime ( spa_data * spa )
{
	printf ( "\tspa_time;%4d/%02d/%02d %02d:%02d:%02f \n", 
					 spa->year,spa->month,spa->day, spa->hour, spa->minute, spa->second );
	return 0;
}

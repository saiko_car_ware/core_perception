/* =================================================== */
/*                                                     */
/*    spa_utils.h for Sun Position Algorithm (SPA)     */
/*                                                     */
/* =================================================== */

#ifndef _spa_utils_h_included_
#define _spa_utils_h_included_

#include "spa.h"

#ifdef __cplusplus
extern "C" {
#endif 

	/* maximum length of Temporal buffer = 10 chars */
#define SPA_MAX_TMPBUFLEN 10

	/* maximum length of message field = 256 chars */
#define SPA_MAX_MESSAGELEN 256

	/* maximum length of message buffer = 1024 chars */
#define SPA_MAX_BUFLEN 1024

	/* =============================================
	 *
	 *	Camera Parameters
	 *
	 *  ============================================= */
	typedef struct {
		double f; /* focul length (mm) */
		double sensor_width;  /* (mm) */
		double sensor_height; /* (mm) */
		double vangle_width;  /* (degree) */
		double vangle_height; /* (degree) */
		double roll; /* (degree) */
		double pitch; /* (degree) */
		double azimuth; /* (degree) */
		int  imgW; /* (pix) */
		int  imgH; /* (pix) */
		/* output */
		int  posX; /* Solar X Position in current frame (pix) */
		int  posY; /* Solar Y Position in current frame (pix) */
	} spa_camparam;
	
	
	/* ============= functions ==============================*/

	/*
	 *	spa_init_spa_data -- Initializing spa_data structure
	 *
	 *   You should call this function at least once 
	 *   before calling "spa_send_data" function.
	 */
	int spa_init_spa_data ( spa_data * spa,
													
													double timezone, 
													// Observer time zone (negative west of Greenwich)
													// valid range: -18   to   18 hours,   error code: 8
													
													double pressure,
													// Annual average local pressure [millibars]
													// valid range:    0 to 5000 millibars,       error code: 12
													
													double temperature,
													// Annual average local temperature [degrees Celsius]
													// valid range: -273 to 6000 degrees Celsius, error code; 13
													
													double atmos_refract
													// Atmospheric refraction at sunrise and sunset (0.5667 deg is typical)
													// valid range: -5   to   5 degrees, error code: 16
													);

	
	/*
	 *	spa_init_spa_camparam -- Initializing spa_camparam structure
	 *
	 *   You should call this function at least once 
	 *   before calling "spa_send_data" function.
	 */
	int spa_init_spa_camparam ( spa_camparam * cam,
															double f, /* focul length (mm) */
															double sensor_width,  /* (mm) */
															double sensor_height, /* (mm) */
															int  imgW, /* image width (pix) */
															int  imgH /* image height (pix) */ );
	

	/*
	 *	spa_send_data - sending sensor data and obtain solar position
	 *
	 *   1) This function intend to have the sensor data by an ASCII character string (pointed by buf),
	 *      in the format like;
	 *
	 *    #TIMEA,USB1,0,30.5,FINESTEERING,2050,549115.250,02008000,9924,14693;VALID,-1.547818057e-09,5.033080928e-10,-18.00000000000,2019,4,27,8,31,37250,VALID*24aead44\r\
	 *    $PASHR,083137.25,289.77,T,-0.78,-0.27,+0.00,0.029,0.030,0.108,2,1*09\r\n"
	 *    #INSPVAXA,USB1,0,30.5,FINESTEERING,2050,549115.250,02008000,471d,14693;INS_SOLUTION_GOOD,INS_RTKFIXED,36.20314793267,139.23863512629,59.2411,41.1000,4.0116,-11.3481,-0.0269,-0.775748679,-0.270424171,289.765001864,0.0235,0.0236,0.0348,0.0093,0.0095,0.0108,0.0295,0.0302,0.1082,0f0020d3,0*89001aae\r	\
	 *
	 *   2) When the data tag (the 1st field in the string) is "#TIMEA",
	 *      this function calculates the current solar position using the parameters set 
	 *      in the structure at the moment. 
	 *
	 *   3) When the solar position is calculated successfully,
	 *      the function returns "1" as the return value. And
	 *      you can obtain the solar position in image by 
	 *        cam_param.posX and cam_param.posY
	 *
	 */
	int spa_send_data ( void * buf,
											unsigned long *frames,
											spa_data * spa,
											spa_camparam * cam_param );

	/*
	 *  spa_print_spatime -- print current spa time for solar position calculation.
	 */
	int spa_print_spatime ( spa_data * spa );

	
#ifdef __cplusplus
};
#endif

#endif /* !_spa_utils_h_included_ */
